#!/bin/bash -x

OUTPUT_FILE=${OUTPUT_FILE:-/dev/null}
SCRIPT="${BASH_SOURCE[0]}"
if [[ "$SCRIPT" != './'* ]] && [[ "$SCRIPT" != '/'* ]]; then
	SCRIPT="/bin/bash $script"
fi

P_STATUS="$1"

if [ -z "$P_STATUS" ]; then
	nohup "$SCRIPT" 'CHILD' >"$OUTPUT_FILE" 2>&1 </dev/null &
	exit 0
fi
if [ "$P_STATUS" = 'CHILD' ]; then
	setsid "$SCRIPT" 'DAEMON' >"$OUTPUT_FILE" 2>&1 </dev/null &
	exit 0
fi
if [ "$P_STATUS" != 'DAEMON' ]; then
	echo "assert: '$P_STATUS' != 'DAEMON'"
	exit 0
fi

while true; do
	date
	sleep 1
done
