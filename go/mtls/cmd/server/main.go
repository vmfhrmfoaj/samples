package main

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
)

func main() {
	clientCert, err := os.ReadFile("client.crt")
	if err != nil {
		Exit(err)
	}
	certPool := x509.NewCertPool()
	if ok := certPool.AppendCertsFromPEM(clientCert); !ok {
		Exit(errors.New("failed to append a cert to a cert pool"))
	}

	tlsCfg := &tls.Config{
		ClientCAs:  certPool,
		ClientAuth: tls.RequireAndVerifyClientCert,
	}

	s := &http.Server{
		Addr:      ":8000",
		TLSConfig: tlsCfg,
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Hi?")
	})

	if err := s.ListenAndServeTLS("server.crt", "server.key"); err != nil {
		fmt.Fprintf(os.Stderr, "failed to start a server: %s\n", err.Error())
	}
}

func Exit(err error) {
	fmt.Fprintln(os.Stderr, err.Error())
	os.Exit(1)
}
