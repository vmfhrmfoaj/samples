package main

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

func main() {
	url := "https://localhost:8000/"

	srvCert, err := os.ReadFile("server.crt")
	if err != nil {
		Exit(err)
	}
	certPool := x509.NewCertPool()
	if ok := certPool.AppendCertsFromPEM(srvCert); !ok {
		Exit(errors.New("failed to append a cert to a cert pool"))
	}

	cert, err := tls.LoadX509KeyPair("client.crt", "client.key")
	if err != nil {
		Exit(err)
	}

	c := http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				RootCAs: certPool, // you can use `InsecureSkipVerify` instead of it.
				// NOTE
				//  `Certificates` doesn't work with some certificate files. (Issuer)
				//  See `getClientCertificate` in ${GOSRC}/src/crypto/tls/handshake_client.go for details
				// Certificates: []tls.Certificate{cert},
				GetClientCertificate: func(cri *tls.CertificateRequestInfo) (*tls.Certificate, error) {
					return &cert, nil
				},
			},
		},
	}

	var resp *http.Response
	for range 3 {
		if resp, err = c.Get(url); err == nil {
			break
		}
		time.Sleep(50 * time.Millisecond)
	}
	if err != nil {
		Exit(err)
	}
	defer resp.Body.Close()

	fmt.Println("Response status:", resp.Status)
	msg, err := io.ReadAll(resp.Body)
	if err != nil {
		Exit(err)
	}

	fmt.Println("Msg:", string(msg))
}

func Exit(err error) {
	fmt.Fprintln(os.Stderr, err.Error())
	os.Exit(1)
}
