ALTER TABLE Todo ADD UpdatedAt DATETIME DEFAULT (DATETIME('now', 'localtime'));
ALTER TABLE Todo ADD CreatedAt DATETIME DEFAULT (DATETIME('now', 'localtime'));

CREATE TRIGGER [TodoUpdatedAt]
AFTER UPDATE
ON Todo
FOR EACH ROW
BEGIN
UPDATE Todo SET UpdatedAt = DATETIME('now', 'localtime') WHERE Id = old.Id;
END;
