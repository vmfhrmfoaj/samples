CREATE TABLE IF NOT EXISTS Todo (
    Id   INTEGER PRIMARY KEY AUTOINCREMENT,
    Done INTEGER CHECK(Done IN (0, 1)) DEFAULT 0,
    Txt  TEXT NOT NULL
);

-- INSERT INTO Todo (Txt) VALUES ('Test 1');
-- INSERT INTO Todo (Txt) VALUES ('Test 2');
