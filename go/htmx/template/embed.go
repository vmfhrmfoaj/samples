package tmpl

import (
	_ "embed"
)

//go:embed index.tmpl
var IndexPage string

//go:embed status-bar.tmpl
var StatusBarSnippet string

//go:embed todolist.tmpl
var TodoListSnippet string

//go:embed todo-status-bar.tmpl
var TodoStatusBarSnippet string

//go:embed todo.tmpl
var TodoSnippet string
