package common

import (
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type Route struct {
	Target    string
	NewRouter func(rs Resource) chi.Router
}

// `SpecialRoutes` is used to register conditional routers.
// For example, It is used for registering "/dev" APIs on `dev` build(i.e. `dev` build tag).
//
// You can register your routers in `init` function.
var SpecialRoutes = []Route{}

type Resource struct {
	ds DataStorage
}

// Return a new API context.
func NewResource(ds DataStorage) Resource {
	return Resource{ds}
}

// Return an error code.
func errToHttpStatus(err error) int {
	if errors.Is(err, ErrInvalidId) {
		return http.StatusBadRequest
	} else {
		return http.StatusInternalServerError
	}
}
