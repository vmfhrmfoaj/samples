package common

import (
	"database/sql"
	"fmt"
	"os"
	"testing"

	"github.com/golang-migrate/migrate/v4/source"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/mattn/go-sqlite3"
)

// Create a test DB. If a db file is existing, `CreateTestDb` will delete the DB file.
func CreateTestDb(srcDir string, dbPath string) error {
	os.Remove(dbPath)

	db, err := sql.Open("sqlite3", "file:"+dbPath)
	if err != nil {
		return fmt.Errorf("unable to open a db file(%s): %w", dbPath, err)
	}
	defer db.Close()
	if err := db.Ping(); err != nil {
		return fmt.Errorf("unable to open a db file(%s): %w", dbPath, err)
	}

	drv, err := source.Open("file://" + srcDir)
	if err != nil {
		return fmt.Errorf("unable to open a migration source directory(%s): %w", srcDir, err)
	}
	defer drv.Close()

	migrate := func(ver uint) error {
		r, _, err := drv.ReadUp(ver)
		if err != nil {
			return fmt.Errorf("unable to open a migration source file(%d): %w", ver, err)
		}
		defer r.Close()

		buf := make([]byte, 10240)
		if _, err = r.Read(buf); err != nil {
			return fmt.Errorf("unable to read a migration source file(%d): %w", ver, err)
		}
		if _, err := db.Exec(string(buf)); err != nil {
			return fmt.Errorf("failed to apply a migration source file(%d): %w", ver, err)
		}

		return nil
	}

	ver, err := drv.First()
	if err != nil {
		return fmt.Errorf("it seems that there is no migration source files: %w", err)
	}

	for {
		if err := migrate(ver); err != nil {
			return err
		}
		ver, err = drv.Next(ver)
		if err != nil {
			break
		}
	}

	return nil
}

func SetupTestDb(initialData []Todo) (db string, ds DataStorage, err error) {
	f, err := os.CreateTemp("/tmp", "htmx-sample-test-")
	if err != nil {
		return
	}
	f.Close()

	db = f.Name()
	err = CreateTestDb("../migration", db)
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			os.Remove(db)
		}
	}()

	ds, err = NewDataStorage(db)
	if err != nil {
		return
	}
	defer func() {
		if err != nil {
			ds.Close()
		}
	}()

	for i, t := range initialData {
		var id int64
		id, err = ds.AddTodo(t.Txt)
		if err != nil {
			return
		}
		if t.Done {
			err = ds.UpdateTodoStatus(id, true)
			if err != nil {
				return
			}
		}

		initialData[i].Id = id
	}

	return
}

func TestNewDataStorage(t *testing.T) {
	db, ds, err := SetupTestDb(nil)
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(db)
	defer ds.Close()

	if !ds.IsAvailable() {
		t.Error("Data source is not available")
	}
}
