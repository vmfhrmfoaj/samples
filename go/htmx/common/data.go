package common

import (
	"errors"
	"fmt"
	"math"
)

type Todo struct {
	Id   int64
	Done bool
	Txt  string
}

var MaxId int64 = math.MaxInt64
var NoLimit = -1

var ErrInvalidId = errors.New("there is no data matching the given id")

// Add a todo into the `Todo` table.
func (ds DataStorage) AddTodo(txt string) (id int64, err error) {
	tx, err := ds.db.Begin()
	if err != nil {
		return 0, fmt.Errorf("unable to lock '%s': %w", ds.name, err)
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	res, err := tx.Exec("INSERT INTO Todo (Txt) VALUES ($1)", txt)
	if err != nil {
		return 0, fmt.Errorf("unable to add a todo into '%s': %w", ds.name, err)
	}

	id, err = res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("unable to get the id: %w", err)
	}

	if err != nil {
		return 0, fmt.Errorf("unable to save a change to '%s': %w", ds.name, err)
	}
	return
}

// Toggle the `Done` column of a row in which the `Id` of the row in the `Todo` table equals the given ID.
func (ds DataStorage) ToggleTodoStatus(id int64) (err error) {
	tx, err := ds.db.Begin()
	if err != nil {
		return fmt.Errorf("unable to lock '%s': %w", ds.name, err)
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	res, err := tx.Exec("UPDATE Todo SET Done = 1 - Done WHERE Id = $1", id)
	if err != nil {
		return fmt.Errorf("unable to update data which id is %d in '%s': %w", id, ds.name, err)
	}

	n, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("unable to get the number of data affected: %w", err)
	}
	if n <= 0 {
		return fmt.Errorf("%w: %d", ErrInvalidId, id)
	}
	return nil
}

// Update the `Done` column of a row in which the `Id` of the row in the `Todo` table equals the given ID.
func (ds DataStorage) UpdateTodoStatus(id int64, done bool) (err error) {
	tx, err := ds.db.Begin()
	if err != nil {
		return fmt.Errorf("unable to lock '%s': %w", ds.name, err)
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	res, err := tx.Exec("UPDATE Todo SET Done = $1 WHERE Id = $2", done, id)
	if err != nil {
		return fmt.Errorf("unable to update data which id is %d in '%s': %w", id, ds.name, err)
	}

	n, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("unable to get the number of data affected: %w", err)
	}
	if n <= 0 {
		return fmt.Errorf("%w: %d", ErrInvalidId, id)
	}
	return nil
}

// Update the `Done` column of all rows in the `Todo`.
func (ds DataStorage) UpdateTodoStatuses(done bool) (err error) {
	tx, err := ds.db.Begin()
	if err != nil {
		return fmt.Errorf("unable to lock '%s': %w", ds.name, err)
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	_, err = tx.Exec("UPDATE Todo SET Done = $1", done)
	if err != nil {
		return fmt.Errorf("unable to update data in '%s': %w", ds.name, err)
	}
	return nil
}

// Update the `Txt` column of a row in which the `Id` of the row in the `Todo` table equals the given ID.
func (ds DataStorage) UpdateTodoText(id int64, txt string) (err error) {
	tx, err := ds.db.Begin()
	if err != nil {
		return fmt.Errorf("unable to lock '%s': %w", ds.name, err)
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	res, err := tx.Exec("UPDATE Todo SET Txt = $2 WHERE Id = $1", txt, id)
	if err != nil {
		return fmt.Errorf("unable to update data which id is %d in '%s': %w", id, ds.name, err)
	}

	n, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("unable to get the number of rows affected: %w", err)
	}
	if n <= 0 {
		return fmt.Errorf("%w: %d", ErrInvalidId, id)
	}
	return nil
}

// Return `n` todos that larger than `id` in `Todo` table.
func (ds DataStorage) GetTodos(lastId int64, n int) ([]Todo, error) {
	if lastId < 0 {
		lastId = math.MaxInt64
	}

	q := "SELECT Id,Done,Txt FROM Todo WHERE Id < $1 ORDER BY Id DESC"
	if n > 0 {
		q += fmt.Sprintf(" LIMIT %d", n)
	}
	rows, err := ds.db.Query(q, lastId)
	if err != nil {
		return nil, fmt.Errorf("unable to get data from '%s': %w", ds.name, err)
	}
	defer rows.Close()

	tl := []Todo{}
	for rows.Next() {
		var t Todo
		err = rows.Scan(&t.Id, &t.Done, &t.Txt)
		if err != nil {
			return nil, fmt.Errorf("unable to parse data of '%s': %w", ds.name, err)
		}

		tl = append(tl, t)
	}

	return tl, nil
}

// Return `n` todos that larger than `id` in `Todo` table.
func (ds DataStorage) GetActiveTodos(lastId int64, n int) ([]Todo, error) {
	if lastId < 0 {
		lastId = math.MaxInt64
	}

	q := "SELECT Id,Done,Txt FROM Todo WHERE Id < $1 AND Done = 0 ORDER BY Id DESC"
	if n > 0 {
		q += fmt.Sprintf(" LIMIT %d", n)
	}
	rows, err := ds.db.Query(q, lastId)
	if err != nil {
		return nil, fmt.Errorf("unable to get data from '%s': %w", ds.name, err)
	}
	defer rows.Close()

	tl := []Todo{}
	for rows.Next() {
		var t Todo
		err = rows.Scan(&t.Id, &t.Done, &t.Txt)
		if err != nil {
			return nil, fmt.Errorf("unable to parse data of '%s': %w", ds.name, err)
		}

		tl = append(tl, t)
	}

	return tl, nil
}

// Reeturn the number of active todos in `Todo` table.
func (ds DataStorage) GetNumActiveTodos() (int, error) {
	q := "SELECT COUNT(Id) FROM Todo WHERE Done = 0"
	row, err := ds.db.Query(q)
	if err != nil {
		return 0, fmt.Errorf("unable to get data from '%s': %w", ds.name, err)
	}
	defer row.Close()

	if !row.Next() {
		return 0, nil
	}

	var n int
	err = row.Scan(&n)
	if err != nil {
		return 0, fmt.Errorf("unable to parse data of '%s': %w", ds.name, err)
	}
	return n, nil
}

// Return `n` todos that larger than `id` in `Todo` table.
func (ds DataStorage) GetCompletedTodos(lastId int64, n int) ([]Todo, error) {
	if lastId < 0 {
		lastId = math.MaxInt64
	}

	q := "SELECT Id,Done,Txt FROM Todo WHERE Id < $1 AND Done = 1 ORDER BY Id DESC"
	if n > 0 {
		q += fmt.Sprintf(" LIMIT %d", n)
	}
	rows, err := ds.db.Query(q, lastId)
	if err != nil {
		return nil, fmt.Errorf("unable to get data from '%s': %w", ds.name, err)
	}
	defer rows.Close()

	tl := []Todo{}
	for rows.Next() {
		var t Todo
		err = rows.Scan(&t.Id, &t.Done, &t.Txt)
		if err != nil {
			return nil, fmt.Errorf("unable to parse data of '%s': %w", ds.name, err)
		}

		tl = append(tl, t)
	}

	return tl, nil
}

// Return a todo in which the `Id` of the row in the `Todo` table equals the given ID.
func (ds DataStorage) GetTodo(id int64) (Todo, error) {
	row, err := ds.db.Query("SELECT Id,Done,Txt FROM Todo WHERE Id = $1", id)
	if err != nil {
		return Todo{}, fmt.Errorf("unable to get data from '%s': %w", ds.name, err)
	}
	defer row.Close()

	if !row.Next() {
		return Todo{}, fmt.Errorf("%w: %d", ErrInvalidId, id)
	}

	var t Todo
	err = row.Scan(&t.Id, &t.Done, &t.Txt)
	if err != nil {
		return Todo{}, fmt.Errorf("unable to parse data of '%s': %w", ds.name, err)
	}
	return t, nil
}

// Remove a todo from the `Todo` table by the given ID.
func (ds DataStorage) RemoveTodo(id int64) (err error) {
	tx, err := ds.db.Begin()
	if err != nil {
		return fmt.Errorf("unable to lock '%s': %w", ds.name, err)
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	res, err := tx.Exec("DELETE FROM todo WHERE id = $1", id)
	if err != nil {
		return fmt.Errorf("unable to remove data from '%s': %w", ds.name, err)
	}

	n, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("unable to get the number of data affected: %w", err)
	}
	if n <= 0 {
		return ErrInvalidId
	}

	return nil
}

// Remove completed todos from the `Todo` table.
func (ds DataStorage) RemoveCompletedTodos() (err error) {
	tx, err := ds.db.Begin()
	if err != nil {
		return fmt.Errorf("unable to lock '%s': %w", ds.name, err)
	}
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	_, err = tx.Exec("DELETE FROM todo WHERE Done = 1")
	if err != nil {
		return fmt.Errorf("unable to remove data from '%s': %w", ds.name, err)
	}

	return nil
}

func NumActiveTodosIn(ts []Todo) (n int) {
	for _, t := range ts {
		if !t.Done {
			n++
		}
	}
	return
}
