package common

import (
	"html/template"
	"log"
	"log/slog"
	"net/http"
	"os"
	"strconv"

	"github.com/go-chi/chi/v5"

	tmpl "jinseop.kim/go/samples/htmx/template"
)

var todoTmpl *template.Template

func init() {
	todoTmpl = template.New("index")
	for _, s := range []string{
		tmpl.IndexPage,
		tmpl.TodoListSnippet,
		tmpl.TodoSnippet,
		tmpl.StatusBarSnippet,
	} {
		if _, err := todoTmpl.Parse(s); err != nil {
			slog.Error("unable to load template files", "error", err)
			os.Exit(1)
		}
	}
}

// Return a router group for `todo` APIs.
func TodoRoutes(rs Resource) chi.Router {
	r := chi.NewRouter()
	r.Post("/add", addTodoFn(rs))
	r.Post("/{id}/toggle", toggleFn(rs))
	r.Get("/{id}/edit", editTodoFn(rs))
	r.Put("/{id}/edit/done", updateTodoFn(rs))
	r.Post("/{id}/edit/cancel", cancelTodoUpdateFn(rs))
	r.Delete("/{id}", deleteTodoFn(rs))

	return r
}

// Return a function that handles a request to create a new todo.
func addTodoFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		if err := r.ParseForm(); err != nil {
			log.Println("unable to parse form:", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		txt := r.Form.Get("text")
		if txt == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		id, err := ds.AddTodo(txt)
		if err != nil {
			log.Println("unable to add todo item:", err)
			w.WriteHeader(errToHttpStatus(err))
			return
		}

		t, err := ds.GetTodo(id)
		if err != nil {
			log.Println("unable to get todo:", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		n, err := ds.GetNumActiveTodos()
		if err != nil {
			log.Println("unable to get num active todos:", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if err := todoTmpl.Execute(w, &struct {
			N    int
			Todo Todo
		}{
			N:    n,
			Todo: t,
		}); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// Return a function that handles a request to toggle the todo state.
func toggleFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		x := chi.URLParam(r, "id")
		id, err := strconv.ParseInt(x, 10, 64)
		if err != nil {
			log.Printf("unable to convert id(%s) to the number: %s\n", x, err)
			w.WriteHeader(http.StatusBadRequest)
		}

		err = ds.ToggleTodoStatus(int64(id))
		if err != nil {
			log.Println("unable to toggle todo status:", err)
			w.WriteHeader(errToHttpStatus(err))
			return
		}

		t, err := ds.GetTodo(id)
		if err != nil {
			log.Println("unable to get todo:", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		n, err := ds.GetNumActiveTodos()
		if err != nil {
			log.Println("unable to get the number of active todo items:", err)
			w.WriteHeader(http.StatusInternalServerError)
		}

		if err := todoTmpl.Execute(w, &struct {
			N    int
			Todo Todo
		}{
			N:    n,
			Todo: t,
		}); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// Return a function that handles a request to start edit th todo text.
func editTodoFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		x := chi.URLParam(r, "id")
		id, err := strconv.ParseInt(x, 10, 64)
		if err != nil {
			log.Println("unable to parse id:", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		t, err := ds.GetTodo(id)
		if err != nil {
			log.Println("unable to get todo:", err)
			w.WriteHeader(errToHttpStatus(err))
			return
		}

		if err := todoTmpl.ExecuteTemplate(w, "editable-todo", t); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// Return a function that handles a request to apply changes of the todo text.
func updateTodoFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		x := chi.URLParam(r, "id")
		id, err := strconv.ParseInt(x, 10, 64)
		if err != nil {
			log.Println("unable to parse id:", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if err := r.ParseForm(); err != nil {
			log.Println("unable to parse form:", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		x = r.Form.Get("text")
		if x == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if err := ds.UpdateTodoText(id, x); err != nil {
			log.Println("unable to update todo text:", err)
			w.WriteHeader(errToHttpStatus(err))
			return
		}

		t, err := ds.GetTodo(id)
		if err != nil {
			log.Println("unable to get todo:", err)
			w.WriteHeader(errToHttpStatus(err))
			return
		}
		if err := todoTmpl.ExecuteTemplate(w, "todo", t); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// Return a function that handles a request to cancel changes of the todo text.
func cancelTodoUpdateFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		x := chi.URLParam(r, "id")
		id, err := strconv.ParseInt(x, 10, 64)
		if err != nil {
			log.Printf("unable to convert id(%s) to the number: %s\n", x, err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		t, err := ds.GetTodo(id)
		if err != nil {
			log.Println("unable to get todo:", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if err := todoTmpl.ExecuteTemplate(w, "todo", t); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// Return a function that handles a request to delete the todo item.
func deleteTodoFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		x := chi.URLParam(r, "id")
		id, err := strconv.ParseInt(x, 10, 64)
		if err != nil {
			log.Printf("unable to convert id(%s) to the number: %s\n", x, err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		err = ds.RemoveTodo(int64(id))
		if err != nil {
			log.Println("unable to delete todo:", err)
			w.WriteHeader(errToHttpStatus(err))
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
