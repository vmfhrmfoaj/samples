package common

import (
	"html/template"
	"log"
	"log/slog"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"

	tmpl "jinseop.kim/go/samples/htmx/template"
)

var todolistTmpl *template.Template

func init() {
	todolistTmpl = template.New("index")
	for _, s := range []string{
		tmpl.IndexPage,
		tmpl.TodoListSnippet,
		tmpl.TodoSnippet,
		tmpl.StatusBarSnippet,
	} {
		if _, err := todolistTmpl.Parse(s); err != nil {
			slog.Error("unable to load template files", "error", err)
			os.Exit(1)
		}
	}
}

// Return a router group for `todo-list` APIs.
func TodoListRouter(rs Resource) chi.Router {
	r := chi.NewRouter()
	r.Get("/all", showAllFn(rs))
	r.Get("/active", showActiveFn(rs))
	r.Get("/completed", showCompletedFn(rs))
	r.Post("/toggle", toggleAllFn(rs))
	r.Delete("/clear-completed", deleteCompletedAllFn(rs))

	return r
}

// Return a function that handles a request to show all todos.
func showAllFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		ts, err := ds.GetTodos(MaxId, NoLimit)
		if err != nil {
			log.Println("unable to get todos:", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		n, err := ds.GetNumActiveTodos()
		if err != nil {
			log.Println("unable to get num active todos:", err)
			n = NumActiveTodosIn(ts)
		}

		if err := todolistTmpl.ExecuteTemplate(w, "todolist-status-bar", &struct {
			N     int
			Todos []Todo
		}{
			N:     n,
			Todos: ts,
		}); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// Return a function that handles a request to show only active todos.
func showActiveFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		ts, err := ds.GetActiveTodos(MaxId, NoLimit)
		if err != nil {
			log.Println("unable to get todos:", err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if err := todolistTmpl.ExecuteTemplate(w, "todolist", ts); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// Return a function that handles a request to show only completed todos.
func showCompletedFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		ts, err := ds.GetCompletedTodos(MaxId, NoLimit)
		if err != nil {
			log.Println("unable to get todos:", err)
			w.WriteHeader(http.StatusInternalServerError)
		}

		if err := todolistTmpl.ExecuteTemplate(w, "todolist", ts); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// Return a function that handles a request to toggle all todos.
func toggleAllFn(rs Resource) http.HandlerFunc {
	ds := rs.ds
	return func(w http.ResponseWriter, r *http.Request) {
		ts, err := ds.GetTodos(MaxId, NoLimit)
		if err != nil {
			log.Println("unable to get todos:", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		if len(ts) == 0 {
			// templ.Handler(template.TodoList(ts, 0)).ServeHTTP(w, r)
			return
		}

		var status bool
		for _, t := range ts {
			if !t.Done {
				status = true
				break
			}
		}
		if err := ds.UpdateTodoStatuses(status); err != nil {
			log.Println("unable to toggle all todos:", err)
			w.WriteHeader(http.StatusInternalServerError)
		}

		for i := range len(ts) {
			ts[i].Done = status
		}
		if err := todolistTmpl.ExecuteTemplate(w, "todolist-status-bar", &struct {
			N     int
			Todos []Todo
		}{
			Todos: ts,
		}); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}

// Return a function that handles a request to delete all completed todos.
func deleteCompletedAllFn(rs Resource) http.HandlerFunc {
	ds := rs.ds

	return func(w http.ResponseWriter, r *http.Request) {
		if err := ds.RemoveCompletedTodos(); err != nil {
			log.Println("unable to delete completed todos:", err)
			w.WriteHeader(http.StatusInternalServerError)
		}

		ts, err := ds.GetTodos(MaxId, NoLimit)
		if err != nil {
			log.Println("unable to get todos:", err)
			w.WriteHeader(http.StatusInternalServerError)
		}

		if err := todolistTmpl.ExecuteTemplate(w, "todolist", ts); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}
}
