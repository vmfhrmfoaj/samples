package common

import (
	"database/sql"
	"errors"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

type DataStorage struct {
	name string
	db   *sql.DB
}

// Initialize the database and Update `db` variable.
func NewDataStorage(dbFile string) (ds DataStorage, err error) {
	ds.name = dbFile
	if ds.db, err = sql.Open("sqlite3", "file:"+dbFile+"?cache=shared&_journal=wal&_timeout=5000"); err != nil {
		return
	}

	if err = ds.db.Ping(); err != nil {
		ds.db.Close()
		ds.db = nil
		return
	}
	return
}

// Returns true if data storage is available, otherwise false.
func (ds DataStorage) IsAvailable() bool {
	if ds.db == nil {
		return false
	}
	if err := ds.db.Ping(); err != nil {
		return false
	}

	return true
}

// Close all resources that this data storage is using.
func (ds DataStorage) Close() error {
	if ds.db == nil {
		return errors.New("a data source was not opened")
	}

	if err := ds.db.Close(); err != nil {
		return fmt.Errorf("failed to close data source: %s", err.Error())
	}
	return nil
}
