package common

import (
	"errors"
	"os"
	"testing"
)

func TestAddAndGetTodo(t *testing.T) {
	db, ds, err := SetupTestDb(nil)
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(db)
	defer ds.Close()

	tcs := []struct {
		Id  int64
		Txt string
	}{
		{Txt: "Test #1"},
		{Txt: "Test #2"},
	}

	for i, tc := range tcs {
		id, err := ds.AddTodo(tc.Txt)
		if err != nil {
			t.Fatal(err)
		}
		tcs[i].Id = id
	}

	for _, tc := range tcs {
		td, err := ds.GetTodo(tc.Id)
		if err != nil {
			t.Error(err)
		}
		if td.Txt != tc.Txt {
			t.Errorf("todo text does not match. expected %s but got %s", tc.Txt, td.Txt)
		}
	}

	_, err = ds.GetTodo(MaxId)
	if err == nil {
		t.Error("expected an error but got nil")
	} else if !errors.Is(err, ErrInvalidId) {
		t.Errorf("expected `ErrInvalidId` but got '%v'", err)
	}
}

func TestGetTodos(t *testing.T) {
	td := []Todo{
		{Txt: "Test 1", Done: true},
		{Txt: "Test 2", Done: false},
		{Txt: "Test 3", Done: false},
		{Txt: "Test 4", Done: true},
	}
	db, ds, err := SetupTestDb(td)
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(db)
	defer ds.Close()

	ts, err := ds.GetTodos(MaxId, NoLimit)
	if err != nil {
		t.Fatal(err)
	}
	ats, err := ds.GetActiveTodos(MaxId, NoLimit)
	if err != nil {
		t.Fatal(err)
	}
	cts, err := ds.GetCompletedTodos(MaxId, NoLimit)
	if err != nil {
		t.Fatal(err)
	}

L:
	for _, a := range td {
		for _, x := range ts {
			if x.Txt == a.Txt && x.Done == a.Done {
				continue L
			}
		}
		t.Errorf("'%s' is not in todo list", a.Txt)

		if a.Done {
			for _, x := range cts {
				if x.Txt == a.Txt {
					continue L
				}
			}
			t.Errorf("'%s' is not in completed todo list", a.Txt)
		} else {
			for _, x := range ats {
				if x.Txt == a.Txt {
					continue L
				}
			}
			t.Errorf("'%s' is not in active todo list", a.Txt)
		}
	}
}

func TestToggleTodoStatus(t *testing.T) {
	td := []Todo{
		{Txt: "Test 1"},
		{Txt: "Test 2"},
		{Txt: "Test 3"},
	}
	db, ds, err := SetupTestDb(td)
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(db)
	defer ds.Close()

	tcs := []struct {
		N        uint
		Expected bool
	}{
		{N: 3, Expected: true},
		{N: 2, Expected: false},
		{N: 1, Expected: true},
	}
	if len(td) < len(tcs) {
		t.Fatal("not enough test data")
	}

	for i, tc := range tcs {
		for range tc.N {
			if err := ds.ToggleTodoStatus(td[i].Id); err != nil {
				t.Error(err)
			}
		}

		td, err := ds.GetTodo(td[i].Id)
		if err != nil {
			t.Error(err)
		}
		if td.Done != tc.Expected {
			t.Errorf("todo status doesn't match. expected %t, but got %t", tc.Expected, td.Done)
		}
	}

	err = ds.ToggleTodoStatus(MaxId)
	if err == nil {
		t.Error("expected an error but got nil")
	} else if !errors.Is(err, ErrInvalidId) {
		t.Errorf("expected `ErrInvalidId` but got '%v'", err)
	}
}

func TestUpdateTodoText(t *testing.T) {
	td := []Todo{
		{Txt: "Test 1"},
		{Txt: "Test 2"},
	}
	db, ds, err := SetupTestDb(td)
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(db)
	defer ds.Close()

	tcs := []struct {
		NewTxt string
	}{
		{NewTxt: "Test 1 (edited)"},
		{NewTxt: "Test 2"},
	}
	if len(td) < len(tcs) {
		t.Fatal("not enough test data")
	}

	for i, tc := range tcs {
		if err := ds.UpdateTodoText(td[i].Id, tc.NewTxt); err != nil {
			t.Error(err)
		}

		td, err := ds.GetTodo(td[i].Id)
		if err != nil {
			t.Error(err)
		}
		if td.Txt != tc.NewTxt {
			t.Errorf("todo text doesn't match. expected '%s', but got '%s'", tc.NewTxt, td.Txt)
		}
	}

	err = ds.UpdateTodoText(MaxId, "...")
	if err == nil {
		t.Error("expected an error but got nil")
	} else if !errors.Is(err, ErrInvalidId) {
		t.Errorf("expected `ErrInvalidId` but got '%v'", err)
	}
}

func TestUpdateTodoStatus(t *testing.T) {
	td := []Todo{
		{Txt: "Test 1"},
		{Txt: "Test 2"},
		{Txt: "Test 3"},
	}
	db, ds, err := SetupTestDb(td)
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(db)
	defer ds.Close()

	tcs := []struct {
		Status bool
	}{
		{Status: true},
		{Status: false},
		{Status: true},
	}
	if len(td) < len(tcs) {
		t.Fatal("not enough test data")
	}

	for i, tc := range tcs {
		if err := ds.UpdateTodoStatus(td[i].Id, tc.Status); err != nil {
			t.Error(err)
		}

		td, err := ds.GetTodo(td[i].Id)
		if err != nil {
			t.Error(err)
		}
		if td.Done != tc.Status {
			t.Errorf("todo status doesn't match. expected %t, but got %t", tc.Status, td.Done)
		}
	}

	err = ds.UpdateTodoStatus(MaxId, true)
	if err == nil {
		t.Error("expected an error but got nil")
	} else if !errors.Is(err, ErrInvalidId) {
		t.Errorf("expected `ErrInvalidId` but got '%v'", err)
	}
}

func TestRemoveTodo(t *testing.T) {
	td := []Todo{
		{Txt: "Test 1"},
		{Txt: "Test 2"},
		{Txt: "Test 3"},
	}
	db, ds, err := SetupTestDb(td)
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(db)
	defer ds.Close()

	tcs := []struct {
		Remove bool
	}{
		{Remove: false},
		{Remove: true},
		{Remove: false},
	}
	if len(td) < len(tcs) {
		t.Fatal("not enough test data")
	}

	for i, tc := range tcs {
		if tc.Remove {
			if err := ds.RemoveTodo(td[i].Id); err != nil {
				t.Error(err)
				continue
			}

			// `RemoveTodo` should delete a specific todo item.
			_, err := ds.GetTodo(td[i].Id)
			if err == nil {
				t.Errorf("'%s' has not been removed", td[i].Txt)
				continue
			}
			if !errors.Is(err, ErrInvalidId) {
				t.Error("")
				continue
			}
		}
	}

	// `RemoveTodo` should not delete other todo items.
	for i, tc := range tcs {
		if !tc.Remove {
			if _, err := ds.GetTodo(td[i].Id); err != nil {
				t.Error(err)
			}
		}
	}
}
