package main

import (
	"os"
	"testing"
)

func main() {
	//---

	os.Args = []string{"-test.v"}
	testing.Main(
		nil,
		[]testing.InternalTest{
			// NOTE
			//  you need to add functional tests here.
		},
		nil, nil,
	)
}
