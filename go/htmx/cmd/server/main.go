package main

import (
	"log"
	"log/slog"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	_ "github.com/mattn/go-sqlite3"

	"jinseop.kim/go/samples/htmx/common"
	"jinseop.kim/go/samples/htmx/env"
	tmpl "jinseop.kim/go/samples/htmx/template"
)

var indexTmpl *template.Template

func init() {
	indexTmpl = template.New("index")
	for _, s := range []string{
		tmpl.IndexPage,
		tmpl.TodoListSnippet,
		tmpl.TodoSnippet,
		tmpl.StatusBarSnippet,
	} {
		if _, err := indexTmpl.Parse(s); err != nil {
			slog.Error("unable to load template files", "error", err)
			os.Exit(1)
		}
	}
}

func main() {
	ds, err := common.NewDataStorage("db.sqlite3")
	if err != nil {
		slog.Error("unable to init data", "error", err)
		os.Exit(1)
	}

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		ts, err := ds.GetTodos(common.MaxId, common.NoLimit)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		n, err := ds.GetNumActiveTodos()
		if err != nil {
			log.Println("unable to get num active todos:", err)
			n = common.NumActiveTodosIn(ts)
		}

		if err := indexTmpl.Execute(w, &struct {
			Dev   bool
			Title string
			N     int
			Todos []common.Todo
		}{
			Dev:   env.Dev,
			Title: "todo",
			N:     n,
			Todos: ts,
		}); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	})

	wd, err := os.Getwd()
	if err != nil {
		log.Println("unable to get working directory:", err)
		os.Exit(1)
	}
	root := http.Dir(filepath.Join(wd, "/public"))
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		ctx := chi.RouteContext(r.Context())
		p := strings.TrimSuffix(ctx.RoutePattern(), "/*")
		fs := http.StripPrefix(p, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})

	rs := common.NewResource(ds)
	for _, sr := range common.SpecialRoutes {
		r.Mount(sr.Target, sr.NewRouter(rs))
	}
	r.Mount("/todo", common.TodoRoutes(rs))
	r.Mount("/todo-list", common.TodoListRouter(rs))

	err = http.ListenAndServe(":3000", r)
	if err != nil {
		log.Println("unable to start server:", err)
		os.Exit(1)
	}
}
