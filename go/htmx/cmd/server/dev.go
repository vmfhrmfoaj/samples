//go:build dev

package main

import (
	"github.com/go-chi/chi/v5"
	"github.com/gorilla/websocket"

	"jinseop.kim/go/samples/htmx/common"
)

var BuildVer string

// WebSockets for development
var DevChs []*websocket.Conn

type DevMsg struct {
	Cmd    string   `json:"cmd"`
	Params []string `json:"params"`
}

func init() {
	common.SpecialRoutes = append(common.SpecialRoutes, common.Route{
		Target:    "/dev",
		NewRouter: DevRouters,
	})
}

func DevRouters(rs common.Resource) chi.Router {
	r := chi.NewRouter()

	return r
}
