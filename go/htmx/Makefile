SHELL = /bin/bash -o pipefail
ORIG_MAKECMDGOALS ?= $(MAKECMDGOALS)
ifneq (,$(wildcard ./.env))
  include .env
ifneq (,$(GOARCH))
  CC := $(CC_$(GOARCH))
endif
  export
endif

BUILD_DIR := ./build
BUILD_TAG ?=
CC        ?=
GOPATH    ?= $(HOME)/go
GCFLAGS   ?=
GLDFLAGS  ?=
RUN_OPTS  ?=
ROOT_DIR  := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

SERVER_EXEC_NAME := server
SERVER_EXEC_FILE := $(BUILD_DIR)/$(SERVER_EXEC_NAME)
SERVER_SRC_FILES := $(shell find cmd/$(SERVER_EXEC_NAME) -name '*.go')

FN_TEST_EXEC_NAME := fn-test
FN_TEST_EXEC_FILE := $(BUILD_DIR)/$(FN_TEST_EXEC_NAME)
FN_TEST_SRC_FILES := $(shell find cmd/$(FN_TEST_EXEC_NAME) -name '*.go')

COMMON_SRC_FILES := $(shell find common    -name '*.go')
SQL_FILES        := $(shell find migration -name '*.sql')
TMPL_SRC_FILES   := $(shell find template  -name '*.tmpl')
IGNORED_FILES    := $(shell git ls-files -coi --exclude-standard | grep -vE '^(build|node_modules)' | tr '\n' ',' | sed 's/,$$//')
SERVER_DB_FILE   := $(BUILD_DIR)/db.sqlite3
STATIC_FILES     := $(patsubst %,build/%,$(SQL_FILES)) $(BUILD_DIR)/public/htmx.min.js $(BUILD_DIR)/public/hyperscript.min.js $(BUILD_DIR)/public/output.css

TEST_SERVER     ?=
TEST_EXTRA_OPTS ?=
DOCKER_IMAGE    ?=
MIGRATE_DB      ?= 'sqlite3://$(SERVER_DB_FILE)'


.PHONY: help
help:  ## List available Make commands (PHONY targets)
	@grep -hIE '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: inspect
inspect:  ## Print the value of variable X
ifndef X
	echo 'usage: make inspect X=SERVER_EXEC_NAME'
else
	@echo $($(X))
endif

.PHONY: clean
clean:  ## Remove generated files
	rm -rf $(BUILD_DIR)

.PHONY: deps
deps: go.mod  ## Download dependencies
	go mod download

define GO_BUILD =
@test -n "$$CGO_ENABLED" && test 0 -eq $$CGO_ENABLED && { \
	echo 'go-sqlite3 requires `CGO`'; \
	exit 1; } || true
CGO_ENABLED=1 go build -gcflags='$(GOCFLAGS)' -ldflags='$(GLDFLAGS)' -tags='$(BUILD_TAG)' -trimpath -o $@ ./cmd/$(notdir $@) 2>&1 | tee .build.out || { \
	[[ '$(ORIG_MAKECMDGOALS)' = 'auto-run' &&  -f $(ROOT_DIR).notify-on-error ]] && bash $(ROOT_DIR).notify-on-error; \
	exit 1; }
endef

.PHONY: build
build: $(SERVER_EXEC_FILE) $(FN_TEST_EXEC_FILE)  ## Build a binary file

$(SERVER_EXEC_FILE): go.mod template/embed.go $(COMMON_SRC_FILES) $(SERVER_SRC_FILES)
	@mkdir -p $(BUILD_DIR)
	$(call GO_BUILD)

$(FN_TEST_EXEC_FILE): go.mod template/embed.go $(COMMON_SRC_FILES) $(FN_TEST_SRC_FILES)
	@mkdir -p $(BUILD_DIR)
	$(eval BUILD_TAG += dev)
	$(call GO_BUILD)

.PHONY: static
static: $(STATIC_FILES)  ## Download or generate asset files

.PHONY: migrate
migrate: $(SERVER_DB_FILE)  ## Build a DB file
$(SERVER_DB_FILE): $(SQL_FILES)
	@$(GOPATH)/bin/migrate --help >/dev/null 2>&1 || { echo "please install `migrate`: go install -tags 'sqlite3' github.com/golang-migrate/migrate/v4/cmd/migrate@latest"; exit 1; }

ifneq (,$(SQL_FILES))
	@mkdir -p $(dir $@)
	$(GOPATH)/bin/migrate -source 'file://migration' -database $(MIGRATE_DB) up || { \
		ver=$$($(GOPATH)/bin/migrate -source 'file://migration' -database $(MIGRATE_DB) version 2>&1 | grep -Eo '^[0-9]+'); \
		$(GOPATH)/bin/migrate -source 'file://migration' -database $(MIGRATE_DB) force $$ver; \
		$(GOPATH)/bin/migrate -source 'file://migration' -database $(MIGRATE_DB) down 1; \
		touch -d "" $@; \
		exit 1; }
	@touch $@
endif

.PHONY: run
run: build migrate static  ## Build a binary, asset and DB file(s). Run functional tests and the binary file
ifeq ($(TEST_SERVER),)
	cd $(dir $(FN_TEST_EXEC_FILE)) && ./$(FN_TEST_EXEC_NAME) $(RUN_OPTS) 2>&1 | tee .fn-test.out || { \
		[[ '$(ORIG_MAKECMDGOALS)' = 'auto-run' && -f $(ROOT_DIR).notify-if-test-fail ]] && bash -x $(ROOT_DIR).notify-if-test-fail; \
		exit 1; }
	cd $(dir $(SERVER_EXEC_FILE)) && ./$(SERVER_EXEC_NAME) $(RUN_OPTS)
else
	scp -r $(BUILD_DIR) "$(TEST_SERVER):"
	ssh $(TEST_SERVER) ./$(FN_TEST_EXEC_NAME) $(RUN_OPTS) 2>&1 | tee .fn-test.out || { \
		[ -f $(ROOT_DIR).notify-on-error ] && bash $(ROOT_DIR).notify-if-test-fail; \
		exit 1; }
	bash -c "\
		trap 'ssh $(TEST_SERVER) pkill -TERM $(SERVER_EXEC_NAME)' SIGINT SIGTERM; \
		ssh $(TEST_SERVER) ./$(SERVER_EXEC_NAME)  $(RUN_OPTS)"
endif

.PHONY: auto-run
auto-run:  ## Build a binary, asset and DB file(s), Run functional tests and the binary file (restart it whenever you change source files)
	@air --help >/dev/null 2>&1 || { echo 'please install `air`: go install github.com/air-verse/air@latest'; exit 1; }
	@echo 'please access http://localhost:3030 for live reloading'

	$(eval BUILD_TAG += dev)
	env BUILD_TAG=$(BUILD_TAG) bash -c "\
		[ -n '$(TEST_SERVER)' ] && trap 'ssh $(TEST_SERVER) pkill -TERM $(SERVER_EXEC_NAME)' SIGINT SIGTERM; \
		[ -z '$(TEST_SERVER)' ] && trap '                   pkill -TERM $(SERVER_EXEC_NAME)' SIGINT SIGTERM; \
		$(GOPATH)/bin/air \
			-build.cmd='env ORIG_MAKECMDGOALS=$@ make build' \
			-build.bin='make' \
			-build.stop_on_error=true \
			-build.args_bin='run' \
			-build.send_interrupt=true \
			-build.delay=0 \
			-build.include_ext='go,tmpl' \
			-build.exclude_dir='build,node_modules' \
			-build.exclude_file='$(IGNORED_FILES)' \
			-build.exclude_regex='_test\.go' \
			-proxy.enabled=true \
			-proxy.proxy_port=3030 \
			-proxy.app_port=3000 \
			-tmp_dir='.tmp'"

.PHONY: test
test: .test  ## Run unit tests
.test: go.mod template/embed.go $(COMMON_SRC_FILES) $(SERVER_SRC_FILES)
	$(eval BUILD_TAG += dev)
	go test -gcflags='$(GCFLAGS)' -ldflags='$(LDFLAGS)' -tags='$(BUILD_TAG)' -v -parallel $(shell nproc) -coverprofile=.coverage.out $(TEST_EXTRA_OPTS) ./...
	@touch $@

.PHONY: fn-test
fn-test: .fn-test  ## Run functional tests
.fn-test: build migrate static
	cd $(dir $(FN_TEST_EXEC_FILE)) && ./$(FN_TEST_EXEC_NAME) $(RUN_OPTS) 2>&1 | tee .fn-test.out || { \
		[[ '$(ORIG_MAKECMDGOALS)' = 'auto-run' && -f $(ROOT_DIR).notify-if-test-fail ]] && bash -x $(ROOT_DIR).notify-if-test-fail; \
		exit 1; }
	@touch $@


template/embed.go: $(TMPL_SRC_FILES)
	@mkdir -p $(dir $@)
	@touch $@ # for re-embeding template files

$(BUILD_DIR)/public/htmx.min.js:
	@mkdir -p $(dir $@)
	wget -O $@ 'https://unpkg.com/htmx.org@latest'

$(BUILD_DIR)/public/hyperscript.min.js:
	@mkdir -p $(dir $@)
	wget -O $@ 'https://unpkg.com/hyperscript.org@latest'

$(BUILD_DIR)/public/output.css: node_modules/tailwindcss $(TMPL_SRC_FILES)
	@mkdir -p $(dir $@)
	npx tailwindcss -m -c tailwind.config.js -i tailwind.input.css -o $(BUILD_DIR)/public/output.css
	touch $@

node_modules/tailwindcss:
	npm install -D tailwindcss
	[ ! -f tailwind.config.js ] && { \
		npx tailwindcss init         \
		sed -i 's@content: \[\],@content: ["template/*.{html,tmpl}"],@' tailwind.config.js }
	[ ! -f tailwind.input.css ] && {                      \
		echo '@tailwind base;'        >tailwind.input.css \
		echo '@tailwind components;' >>tailwind.input.css \
		echo '@tailwind utilities;'  >>tailwind.input.css }

$(BUILD_DIR)/migration/%: migration/%
	@mkdir -p $(dir $@)
	cp $< $@
