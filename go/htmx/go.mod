module jinseop.kim/go/samples/htmx

go 1.22.4

require (
	github.com/go-chi/chi/v5 v5.0.11
	github.com/mattn/go-sqlite3 v1.14.22
)

require (
	github.com/a-h/templ v0.2.543
	github.com/golang-migrate/migrate/v4 v4.17.0
	github.com/gorilla/websocket v1.5.1
)

require golang.org/x/net v0.18.0 // indirect
