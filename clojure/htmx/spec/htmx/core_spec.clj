(ns htmx.core-spec
  (:require [clojure.spec.alpha :as spec]))

(set! *warn-on-reflection* true)


(spec/def ::simple (spec/and string? (set (map str "simple"))))
