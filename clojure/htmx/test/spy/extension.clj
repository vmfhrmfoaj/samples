(ns spy.extension
  (:refer-clojure :exclude [fn]))


(def any
  "An object that returns true when compared to any objects."
  (proxy [java.lang.Object] []
    (equals [_that] true)))


(def fn
  "An object that returns true when compared to functions."
  (proxy [java.lang.Object] []
    (equals [that] (fn? that))))
