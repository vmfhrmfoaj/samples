(ns htmx.core-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as spec]
            [clojure.spec.gen.alpha :as gen]
            [htmx.core :as target]
            [htmx.core-spec :as target-spec]
            [htmx.core-gen]
            [htmx.test-util :as util]
            [spy.core :as spy]))

(use-fixtures :once util/without-trivial-log util/with-db)


(deftest example
  (testing "how to use `spy`"
    (is (let [x (spy/stub nil)]
          (x 1 2 3 4)
          (spy/called-with? x 1 2 3 4))))

  (testing "how to use `clojure.spec.gen`"
    (is (get (->> "simple"
                  (map str)
                  (into #{}))
             (gen/generate (spec/gen ::target-spec/simple))))))
