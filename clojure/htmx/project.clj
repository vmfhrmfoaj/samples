(defproject vmfhrmfoaj/htmx
  "0.1.0-SNAPSHOT"

  :description "FIXME"
  :url "FIXME"
  :license {:name "The MIT License"
            :url "https://opensource.org/licenses/MIT"}

  :main htmx.core

  :source-paths ["src" "spec"]
  :test-paths ["test"]

  :plugins [[migratus-lein "0.7.3"]
            [lein-cljsbuild "1.1.8"]]

  :dependencies [[org.clojure/clojure "1.11.1"]
                 [com.taoensso/timbre "6.1.0"]
                 [com.fzakaria/slf4j-timbre "0.3.21"]
                 [org.clojure/tools.cli "1.0.214"]
                 [com.github.vmfhrmfoaj/clj-toml "1.0.0-SNAPSHOT"]
                 [com.github.seancorfield/honeysql "2.3.928"]
                 [com.github.seancorfield/next.jdbc "1.3.834"]
                 [environ "1.2.0"]
                 [org.clojure/core.async "1.6.673"]
                 [metosin/reitit "0.6.0"]
                 [metosin/reitit-swagger    "0.6.0"]
                 [metosin/reitit-swagger-ui "0.6.0"]
                 [ring/ring-core "1.9.6"] ; required by reitit.ring.middleware
                 [http-kit "2.7.0"]
                 [ring/ring-jetty-adapter "1.9.6"]
                 [hiccup "2.0.0-RC1"]
                 [org.xerial/sqlite-jdbc "3.39.2.1"]]

  :profiles
  {:uberjar {:aot :all
             :jvm-opts ["-Dclojure.compiler.direct-linking=true"]
             ;; NOTE
             ;;  if you globally set `:target-path` to "target/%s", `lein uberjar` command will remove trampoline caches.
             :target-path "target/uberjar"}
   :dev {:source-paths ["gen" "tool"]
         :dependencies [[org.clojure/data.xml "0.0.8"]
                        [org.clojure/test.check "1.1.1"]
                        [org.clojure/tools.deps.alpha "0.14.1222" :exclusions [com.google.guava/guava]]
                        [criterium "0.4.6"]
                        [tortue/spy "2.13.0"]
                        [migratus "1.4.4"]
                        [ring/ring-mock "0.4.0"]
                        [org.clojure/clojurescript "1.11.60"]
                        [haslett "0.1.7"]]
         :repl-options {:init-ns user
                        ;; If you got an error, remove a vector surrounding `do` expression.
                        ;; See, https://github.com/technomancy/leiningen/issues/878
                        :init [(do
                                 (require 'taoensso.timbre)
                                 (taoensso.timbre/merge-config! {:min-level :info ; if you set it to `:debug`, you may see a lot of logs.
                                                                 :timestamp-opts {:timezone (java.util.TimeZone/getDefault)}}))]}
         :migratus {:store :database
                    :migration-dir "data/migrations"
                    :db {:dbtype "sqlite"
                         :dbname ".db"}}
         :test-refresh ^:replace {:changes-only true
                                  :timeout-in-sec 5
                                  :notify-command ["notify-send" "Clojure Test"]
                                  ;; `:level`: send a notification whenever a test result is failure
                                  ;; `:edge`: send a notification if a test result is changed (success -> failure or filaure -> success)
                                  :notify-trigger-type :edge}}
   :cicd {:local-repo ".m2/repository"}}

  :cljsbuild
  {:builds {:dev {:source-paths ["tool"]
                  :compiler {:main dev.tool
                             :asset-path "/static/out"
                             :output-dir "dev-resources/static/out"
                             :output-to  "dev-resources/static/extra.js"
                             :optimizations :none
                             :source-map-timestamp true
                             :warnings {:single-segment-namespace false}
                             :parallel-build true}}}})
