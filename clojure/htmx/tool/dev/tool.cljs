(ns dev.tool
  (:require-macros [cljs.core.async.macros])
  (:require [clojure.edn :as edn]
            [cljs.core.async :as async :refer [go go-loop <!]]
            [haslett.client :as ws]))

(enable-console-print!)


(def ws-status (atom nil))


(defn reload
  "Reload the current page." []
  (.reload js/location))


(go-loop [stream (<! (ws/connect "ws://localhost:3030/ws"))]
  (condp = @ws-status
    :opened
    (do
      (<! (async/timeout 5000))
      (recur stream))

    :closed
    (do
      (println "status:" @ws-status "-> nil")
      (reset! ws-status nil)
      (<! (async/timeout 1000))
      (recur (<! (ws/connect "ws://localhost:3030/ws"))))

    (do
      (println "status:" @ws-status "-> :opened")
      (reset! ws-status :opened)
      (go
        (loop []
          (let [{:keys [source close-status]} stream
                [v ch] (async/alts! [close-status source] :priority true)]
            (cond
              (= close-status ch)
              (println "socket closed:" v)

              (nil? v)
              (println "got nil message, socket seems to be closed")

              (= source ch)
              (let [{:keys [cmd] :as msg} (edn/read-string v)]
                (println "socket msg:" msg)
                (condp = cmd
                  :reload (reload)

                  (println "socket command: unkown command, cmd =" cmd))
                (recur)))))
        (ws/close stream)
        (println "status:" @ws-status "-> :closed")
        (reset! ws-status :closed))
      (recur stream))))
