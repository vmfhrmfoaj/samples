LEIN ?= lein

JAVA_HOME  := $(shell dirname $$(dirname $$(readlink -f $$(which javac))))
GROUP_NAME := $(shell cat project.clj | tr -d '\n' | gawk 'match($$0,/defproject\s+([^ \/]+)\/([^ ]+)\s+"([^"]+)"/,m)  { print m[1] }')
PROJ_NAME  := $(shell cat project.clj | tr -d '\n' | gawk 'match($$0,/defproject\s+([^ \/]+\/)?([^ ]+)\s+"([^"]+)"/,m) { print m[2] }')
PROJ_VER   := $(shell cat project.clj | tr -d '\n' | gawk 'match($$0,/defproject\s+([^ \/]+\/)?([^ ]+)\s+"([^"]+)"/,m) { print m[3] }')

SRC_FILES      := $(shell find . -name '*.clj' -or -name '*.clj[cs]' 2>/dev/null)
RESOURCE_FILES := $(shell find resources -name '*' 2>/dev/null)
JAR_FILE     := target/uberjar/$(PROJ_NAME)-$(PROJ_VER).jar
UBERJAR_FILE := target/uberjar/$(PROJ_NAME)-$(PROJ_VER)-standalone.jar
INSTALLED_JAR_FILE := $(HOME)/.m2/repository/$(GROUP_NAME)/$(PROJ_NAME)/$(PROJ_VER)/$(PROJ_NAME)-$(PROJ_VER).jar


.PHONY: build
build: $(JAR_FILE)

.PHONY: test
test: .test
	$(LEIN) trampoline test-plus ':once'

.PHONY: isntall
install: $(INSTALLED_JAR_FILE)

.PHONY: jar
jar: $(JAR_FILE)

.PHONY: uberjar
uberjar: $(UBERJAR_FILE)

.PHONY: deploy
deploy: clean $(UBERJAR_FILE)
	$(LEIN) deploy

.PHONY: clean
clean:
	$(LEIN) clean


pom.xml: project.clj
	$(LEIN) pom

.test: project.clj $(SRC_FILES) $(RESOURCE_FILES)
	$(LEIN) trampoline test-plus ':once'
	@touch .test

$(JAR_FILE): static project.clj $(SRC_FILES) $(RESOURCE_FILES)
ifneq (install,$(MAKECMDGOALS))
	$(LEIN) jar  # `lein install` will generate a jar file, so don't need generate it here.
endif
	@test -f $@ && touch $@

$(UBERJAR_FILE): static project.clj $(SRC_FILES) $(RESOURCE_FILES)
	$(LEIN) uberjar
	@test -f $@ && touch $@

$(INSTALLED_JAR_FILE): $(JAR_FILE)
	$(LEIN) install
	@test -f $@ && touch $@

.PHONY: static
static: resources/static/tailwind.css resources/static/htmx.min.js resources/static/hyperscript.min.js

resources/static/tailwind.css:
	npm install -D tailwindcss
	npx tailwindcss -m -c .tailwind.config.js -i dev-resources/input.css -o resources/static/tailwind.css

resources/static/htmx.min.js:
	wget -O $@ https://unpkg.com/htmx.org@latest

resources/static/hyperscript.min.js:
	wget -O $@ https://unpkg.com/hyperscript.org@latest
