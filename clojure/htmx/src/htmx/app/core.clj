(ns htmx.app.core
  (:require [hiccup2.core :as h]
            [htmx.app.todo.core :as todo]
            [htmx.app.todo.var :as todo.var]))

(set! *warn-on-reflection* true)


(defn routes
  "Returns a vector that collection of app's routes.

  Examples:
   (routes)
   (routes {...})"
  ([]
   (routes nil))
  ([context]
   [["/index.html"
     {:get {:summary "Index page"
            :responses {200 {:body string?}}
            :handler (let [t (->> [:html
                                   [:head
                                    [:meta {:charset "UTF-8"}]
                                    [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
                                    [:link {:rel "stylesheet" :href "/static/tailwind.css"}]
                                    [:script {:src "/static/htmx.min.js"}]
                                    [:script {:src "/static/hyperscript.min.js"}]
                                    [:script {:src "/static/extra.js"}]]
                                   [:body {:class "bg-black text-gray-200"}
                                    [:div#wrapper {:class "w-1/3 min-w-[600px] mx-auto p-8"}
                                     [:h1 {:class "w-auto pb-16 text-8xl text-center text-red-950"}
                                      "todos"]
                                     "%s"]]]
                                  (h/html)
                                  (str "<!DOCTYPE html>"))]
                       (fn [_param]
                         {:status 200, :body (format t (todo/todo-list (vals @todo.var/todos)))}))}}]
    ["/todo-list"
     ["/all"             (todo/all-todo-list-handler context)]
     ["/active"       (todo/active-todo-list-handler context)]
     ["/completed" (todo/completed-todo-list-handler context)]
     ["/toggle"       (todo/toggle-todo-list-handler context)]]
    ["/todo"
     ["/add" (todo/add-todo-handler context)]
     ["/:id"
      ["/"       (todo/delete-todo-handler context)]
      ["/toggle" (todo/toggle-todo-handler context)]
      ["/edit"
       ["/"         (todo/edit-todo-handler context)]
       ["/done"   (todo/update-todo-handler context)]
       ["/cancel" (todo/cancel-todo-handler context)]]]]]))
