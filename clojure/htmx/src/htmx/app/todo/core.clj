(ns htmx.app.todo.core
  (:require [clojure.string :as str]
            [hiccup2.core :as h]
            [htmx.app.todo.var :as var]
            [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)


(defn todo
  "Returns HTML for todo item.

  Example:
   (todo {:id 0 :done? false, :text \"do something\"})
   (todo {:id 0 :done? false, :text \"do something\"} true) ; use `input` instead of `label` for 'text' field"
  ([t]
   (todo t false))
  ([{:keys [id done? text] :as _todo} editable?]
   (let [chk-style (str " bg-no-repeat bg-left " (if done? "bg-chk-box-checked" "bg-chk-box-unchecked"))]
     (h/html [:li {:id (str "todo-" id)
                   :hx-post (str "/todo/" id "/edit/cancel")
                   :hx-trigger "cancel"
                   :hx-target "this"}
              [:div {:class "inline-flex w-full border-b-2 border-gray-700 items-center"}
               [:p {:class (str "inline-block w-12 h-8 pr-2" chk-style (when editable? " opacity-0"))
                    :hx-post (str "/todo/" id "/toggle")}]
               (let [attr {:class "p-3 text-2xl leading-8"}]
                 (if editable?
                   [:input (merge-with #(str %1 " " %2) attr
                                       {:class "border-2 border-gray-500"
                                        :name "text" :value text
                                        :autofocus "autofocus"
                                        :autocomplete "off"
                                        :hx-put (str "/todo/" id "/edit/done")
                                        :hx-trigger "keyup[keyCode==13]"
                                        :_ (str "on load call my.select()\n"
                                                "on focusout send cancel to #todo-" id)})]
                   [:div (update attr :class str " relative w-full group")
                    [:label (cond-> {:hx-get (str "/todo/" id "/edit")
                                     :hx-trigger "dblclick"}
                              done? (update :class #(str % " line-through")))
                     text]
                    [:button {:class "absolute inset-y-0 right-0 invisible group-hover:visible p-2 text-base text-red-400"
                              :hx-delete (str "/todo/" id "/")
                              :hx-trigger "click"
                              :hx-swap "delete"}
                     "X"]]))]]))))


(defn new-todo
  "Returns HTML for an input form to add a new todo item." []
  (h/html [:div#new-todo {:class "pb-3 mb-1 border-b-2 border-gray-500"
                          :hx-swap-oob "true"}
           [:label {:class "inline-flex items-center"}
            [:p {:class "w-8 h-8 -rotate-90 before:pr-2 before:text-2xl before:content-['❮']"
                 :hx-post "/todo-list/toggle/"
                 :hx-target "#todo-list"}]
            [:input {:class "ml-2 text-2xl bg-black"
                     :name "text"
                     :autofocus "autofocus"
                     :autocomplete "off"
                     :placeholder "What needs to be done?"
                     :hx-post "/todo/add"
                     :hx-trigger "keyup[keyCode==13]"
                     :hx-target "#todo-list"
                     :hx-swap "beforeend"}]]]))


(defn todo-list-bar
  "Returns HTML for todo list bar.

  Example:
   (todo-list-bar {0 {:done? false}, ...})"
  ([]
   (todo-list-bar (vals @var/todos)))
  ([todos]
   (h/html [:div#todo-list-bar {:class "flex justify-between p-2"
                                :hx-swap-oob "true"}
            [:span {:class "m-auto"} (str (count (filterv #(not (:done? %)) todos)) " item left")]
            [:div {:class "inline-flex flex-grow justify-center"}
             [:button {:class "p-1" :hx-get "/todo-list/all"       :hx-target "#todo-list"} "All"]
             [:button {:class "p-1" :hx-get "/todo-list/active"    :hx-target "#todo-list"} "Active"]
             [:button {:class "p-1" :hx-get "/todo-list/completed" :hx-target "#todo-list"} "Completed"]]
            [:button {:hx-delete "/todo-list/completed"
                      :hx-target "#todo-list"}
             "Clear completed"]])))


(defn todo-list
  "Returns HTML for todo list.

  Exmaple:
   (todo-list [{:id 0, :done? false, :text \"do something\"}, ...])"
  [todos]
  (h/html [:div
           (new-todo)
           [:ul#todo-list
            (for [t (sort-by :id todos)]
              (todo t))]
           (todo-list-bar todos)]))

;; ---

(defn- add-todo!
  "Add a todo into the todo list.

  Example:
   (new-todo! \"...\")"
  [text]
  (let [id (var/get-id!)
        t {:id id :done? false, :text text}]
    (swap! var/todos assoc id t)
    t))


(defn add-todo-handler
  "Return a Ring handler for adding to-do.

  Example:
   (new-todo-handler {...})"
  [_context]
  {:post {:summary "Add a new to-do item"
          :parameters {:form {:text string?}}
          :responses {200 {:body string?} 400 {:body {:error string?}}}
          :handler (fn [{{{:keys [text]} :form} :parameters}]
                     (if (str/blank? text)
                       {:status 400, :body {:error "invalid text"}}
                       {:status 200, :body (str (new-todo)
                                                (todo (add-todo! text))
                                                (todo-list-bar))}))}})


(defn- edit-todo!
  "Edit a todo into the todo list.

  Example:
   (edit-todo! 0 \"new\")"
  [id text]
  (get (swap! var/todos #(update % id assoc :text text)) id))


(defn update-todo-handler
  "Return a Ring handler for editing to-do.

  Example:
   (update-todo-handler {...})"
  [_context]
  {:put {:summary "Update a to-do item"
         :parameters {:path {:id int?} :form {:text string?}}
         :responses {200 {:body string?} 400 {:body {:error string?}}}
         :handler (fn [{{{:keys [id]} :path {:keys [text]} :form} :parameters}]
                    (let [t (var/get-todo id)]
                      (cond
                        (nil? t)          {:status 400, :body {:error "invalid id"}}
                        (str/blank? text) {:status 400, :body {:error "wrong todo item"}}
                        :else             {:status 200, :body (str (todo (edit-todo! id text)))})))}})


(defn edit-todo-handler
  "Return a Ring handler for editing to-do.

  Example:
   (edit-todo-handler {...})"
  [_context]
  {:get {:summary "Edit a to-do item"
         :parameters {:path {:id int?}}
         :responses {200 {:body string?} 400 {:body {:error string?}}}
         :handler (fn [{{{:keys [id]} :path} :parameters}]
                    (if-let [t (var/get-todo id)]
                      {:status 200, :body (str (todo t true))}
                      {:status 400, :body {:error "invalid id"}}))}})


(defn cancel-todo-handler
  "Return a Ring handler for canceling to edit to-do.

  Example:
   (edit-todo-handler {...})"
  [_context]
  {:post {:summary "Cancel editing a to-do item"
          :parameters {:path {:id int?}}
          :responses {200 {:body string?} 400 {:body {:error string?}}}
          :handler (fn [{{{:keys [id]} :path} :parameters}]
                     (if-let [t (var/get-todo id)]
                       {:status 200, :body (str (todo t))}
                       {:status 400, :body {:error "invalid id"}}))}})


(defn- toggle-todo!
  "Toggle the completion status of a todo.

  Example:
   (toggle-todo! {:id 0 :done? false})"
  [id]
  (get (swap! var/todos #(update-in % [id :done?] not)) id))


(defn toggle-todo-handler
  "Return a Ring handler for toggling to-do.

  Example:
   (toggle-todo-handler {...})"
  [_context]
  {:post {:summary "Toggle a to-do item"
          :parameters {:path {:id int?}}
          :responses {200 {:body string?} 400 {:body {:error string?}}}
          :handler (fn [{{{:keys [id]} :path} :parameters}]
                     (log/info "toggle" id)
                     (if (var/get-todo id)
                       {:status 200, :body (str (todo (toggle-todo! id)) (todo-list-bar))}
                       {:status 400, :body {:error "invalid id"}}))}})


(defn delete-todo!
  "Delete a todo item.

  Example:
   (delete-todo! 0)"
  [id]
  (swap! var/todos dissoc id)
  true)


(defn delete-todo-handler
  "Return a Ring handler for deleting to-do.

  Example:
   (delete-todo-handler {...})"
  [_context]
  {:delete {:summary "Delete a to-do item"
            :parameters {:path {:id int?}}
            :responses {400 {:body {:error string?}}}
            :handler (fn [{{{:keys [id]} :path} :parameters}]
                       (log/info "delete" id)
                       (if (var/get-todo id)
                         (do
                           (delete-todo! id)
                           {:status 200})
                         {:status 400, :body {:error "invalid id"}}))}})

;; ---

(defn all-todo-list-handler
  "Returns a ring handler for showing all todo items.

  Example:
   (all-todo-list-handler {...})"
  [_context]
  {:get {:summary "Show all todo items."
         :responses {200 {:body string?} 400 {:body {:error string?}}}
         :handler (fn [_param]
                    {:status 200, :body (str (todo-list (vals @var/todos)))})}})


(defn active-todo-list-handler
  "Returns a ring handler for showing only active todo items.

  Example:
   (active-todo-list-handler {...})"
  [_context]
  {:get {:summary "Show only active todo items."
         :responses {200 {:body string?} 400 {:body {:error string?}}}
         :handler (fn [_param]
                    {:status 200, :body (str (todo-list (filter #(not (:done? %)) (vals @var/todos))))})}})


(defn completed-todo-list-handler
  "Returns a ring handler for showing/deleting only completed todo items.

  Example:
   (completed-todo-list-handler {...})"
  [_context]
  {:get {:summary "Show only completed todo items."
         :responses {200 {:body string?} 400 {:body {:error string?}}}
         :handler (fn [_param]
                    {:status 200, :body (str (todo-list (filter :done? (vals @var/todos))))})}

   :delete {:summary "Delete completed todo items."
            :responses {200 {:body string?} 400 {:body {:error string?}}}
            :handler (let [remove-completd (partial reduce-kv #(cond-> %1
                                                                 (not (:done? %3))
                                                                 (assoc %2 %3)) {})]
                       (fn [_param]
                         (let [todos (vals (swap! var/todos remove-completd))]
                           {:status 200, :body (str (todo-list todos))})))}})


(defn toggle-todo-list-handler
  "Returns a ring handler for toggle only completed todo items.

  Example:
   (toggle-todo-list-handler {...})"
  [_context]
  {:post {:summary "Toggle all task items"
          :responses {200 {:body string?} 400 {:body {:error string?}}}
          :handler (let [toggle-all (fn [todos]
                                      (let [done? (not-every? :done? (vals todos))]
                                        (reduce-kv #(assoc %1 %2 (assoc %3 :done? done?)) {} todos)))]
                     (fn [_param]
                       {:status 200, :body (str (todo-list (vals (swap! var/todos toggle-all))))}))}})


(comment
  (reset! var/id 0)
  (reset! var/todos nil)
  ,                  (add-todo! "Test #1")
  (toggle-todo! (:id (add-todo! "Test #2")))
  var/todos)
