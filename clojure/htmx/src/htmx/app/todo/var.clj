(ns ^{:clojure.tools.namespace.repl/load   false
      :clojure.tools.namespace.repl/unload false}
  htmx.app.todo.var)

(set! *warn-on-reflection* true)


(def id (atom 0))

(def todos (atom nil))


(defn get-id!
  "Returns ID and increase ID counter.

  Example:
   (get-id!)" []
  (first (swap-vals! id inc)))


(defn get-todo
  "Returns todo from todo list.

  Example:
   (get-todo 0)"
  [id]
  (get @todos id))
