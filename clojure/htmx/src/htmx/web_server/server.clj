(ns htmx.web-server.server
  (:require [org.httpkit.server :as http-kit]
            [taoensso.timbre :as log])
  (:import java.util.concurrent.Executors))

(set! *warn-on-reflection* true)


(defn start
  "Start a server.

  Example:
   (start #function[...] 3000)"
  [handler port]
  (log/info "starting a server")
  (let [opt {:port port
             :legacy-return-value? false
             :worker-pool (Executors/newVirtualThreadPerTaskExecutor)}
        server (http-kit/run-server handler opt)]
    (when (= :running (http-kit/server-status server))
      (log/info "a server has started")
      server)))


(defn stop
  "Stop a server.

  Example:
   (stop #object[Server])"
  ([server]
   (stop server 2500))
  ([server _timeout]
   (cond
     (not (satisfies? http-kit/IHttpServer server))
     (do
       (log/error "a server should be satisfied 'IHttpServer' protocol")
       false)

     (= #{:stopped :stopping} (http-kit/server-status server))
     (do
       (log/warn "a server is stopping or has already been stopped")
       true)

     :else
     (do
       ;; TODO
       ;;  stop with timeout
       (log/info "stopping a server")
       (http-kit/server-stop! server)))))


(defn running?
  "Return 'true' if the server is running, otherwise return 'false' or 'nil'.

  Example:
   (running? #object[Server])"
  [server]
  (if (satisfies? http-kit/IHttpServer server)
    (= :running (http-kit/server-status server))
    (do
      (log/error "a server should be satisfied 'IHttpServer' protocol")
      false)))
