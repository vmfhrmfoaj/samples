(ns htmx.web-server.core
  (:require [htmx.web-server.server :as server]
            [htmx.web-server.var :as var]
            [reitit.ring :as ring]
            [reitit.coercion.spec]
            [reitit.ring.coercion :as mid.coercion]
            [reitit.ring.middleware.exception :as mid.exception]
            [reitit.ring.middleware.muuntaja :as mid.muuntaja]
            [reitit.ring.middleware.parameters :as mid.params]
            [reitit.swagger :as swagger]
            [reitit.swagger-ui :as swagger-ui]
            [muuntaja.core :as muuntaja]
            [reitit.dev.pretty :as pretty]
            [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)


(defn- default-routes
  [_context]
  (let [redirect (constantly {:status 301 :headers {"Location" "/index.html"}})]
    [["/" {:no-doc true, :get {:handler redirect}}]
     [""  {:no-doc true}
      ["/swagger.json" {:get (swagger/create-swagger-handler)}]
      ["/api-docs/*"   {:get (swagger-ui/create-swagger-ui-handler)}]]]))


(defn routes
  "Return top-level router.

  Examples:
   (routes {:external-asset-path \"...\"})
   (routes {:external-asset-path \"...\"} [[\"/api\" [\"/echo\" {:get {:handler #fn[...]}}]]])"
  ([context]
   (routes context nil))
  ([context sub-routes]
   (into (default-routes context) sub-routes)))


(defn handler
  "Return ring handler.

  Examples:
   (handler {:external-asset-path \"...\"})
   (handler {:external-asset-path \"...\"} [[\"/api\" [\"/echo\" {:get {:handler #fn[...]}}]]])"
  ([context]
   (handler context nil))
  ([context sub-routes]
   (let [router (ring/router (routes context sub-routes)
                             {:exception pretty/exception
                              :data {:coercion reitit.coercion.spec/coercion
                                     :muuntaja muuntaja/instance
                                     :middleware [mid.params/parameters-middleware
                                                  mid.muuntaja/format-negotiate-middleware
                                                  mid.muuntaja/format-response-middleware
                                                  mid.muuntaja/format-request-middleware
                                                  mid.exception/exception-middleware
                                                  mid.coercion/coerce-response-middleware
                                                  mid.coercion/coerce-request-middleware]}})
         handler (ring/routes (ring/redirect-trailing-slash-handler)
                              (ring/create-resource-handler {:root "static/" :path "/static"})
                              (ring/create-default-handler {:not-found (constantly {:status 404 :body "Unknown path"})}))]
     (ring/ring-handler router handler))))


(defn start
  "Start the web server.

  Example:
   (start {:server {:port 3000}} [[\"/api\" [\"/echo\" {:get {:handler #fn[...]}}]]])"
  [context sub-routes]
  (log/debug {:context context :sub-routes sub-routes})
  (let [handler (handler context sub-routes)
        port (get-in context [:server :port] 3000)]
    (try
      (server/start handler port)
      (catch Exception _err
        ;; TODO
        nil))))


(defn start!
  "Start the web server and update `var/server`.

  Example:
   (start! {:server {:port 8080}} [[\"/api\" [\"/echo\" {:get {:handler #fn[...]}}]]])"
  [context routes]
  (when (not @var/server)
    (when-let [new-server (start context routes)]
      (compare-and-set! var/server nil new-server)
      (when (not= @var/server new-server)
        (server/stop new-server))
      true)))


(defn stop
  "Stop the web server.

  Examples:
   (stop (`start` ...))
   (stop (`start` ...) 1000) ; stop with 1000 msecs timeout"
  ([server]
   (stop server nil))
  ([server timeout]
   (log/debug {:server server :timeout timeout})
   (when-let [p (server/stop server timeout)]
     (if-not timeout
       (deref p)
       (deref p timeout :timeout)))))


(defn stop!
  "Stop the web server and update `var/server`.

  Examples:
   (stop!)
   (stop! 1000) ; stop with 1000 msecs timeout"
  ([]
   (stop! nil))
  ([timeout]
   (when-let [[old-server] (reset-vals! var/server nil)]
     (stop old-server timeout))))


(defn running?
  "Return 'true' if the server is running, otherwise return 'false' or 'nil'.

  Example:
   (running?)
   (running? (`start`))"
  ([]
   (running? @var/server))
  ([svr]
   (when svr
     (server/running? svr))))


(comment
  (require 'htmx.app.core)
  (let [ctx nil]
    (start! ctx (htmx.app.core/routes ctx))))
