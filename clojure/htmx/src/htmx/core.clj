(ns htmx.core
  (:gen-class)
  (:require [htmx.app.core :as app]
            [htmx.db.core :as db]
            [htmx.web-server.core :as server]
            [taoensso.timbre :as log])
  (:import java.util.TimeZone))

(set! *warn-on-reflection* true)


(defn context
  "Returns a context map.

  Examples:
   (context)
   (context *command-line-args*)"
  ([]
   (context nil))
  ([_args]
   nil))


(defn -main
  [& args]
  (log/merge-config! {:min-level :warn, :timestamp-opts {:timezone (TimeZone/getDefault)}})
  (db/connect!)
  (let [ctx (context args)]
    (server/start! ctx (app/routes ctx))))


(comment
  (db/connect!)
  (alter-var-root #'clojure.core/*command-line-args* (constantly nil))
  (let [ctx (context *command-line-args*)]
    (server/start! ctx (app/routes ctx))))
