(ns htmx.db.core
  (:require [clojure.string :as str]
            [htmx.db.var :as var]
            [htmx.internal.db-util :refer [merge-queries]]
            [environ.core :refer [env]]
            [next.jdbc :as jdbc]
            [next.jdbc.result-set :as res]
            [taoensso.timbre :as log])
  (:import java.sql.Connection))

(set! *warn-on-reflection* true)


(defn- default-config []
  (let [{db-file "SQLITE_DB_FILE" :or {db-file ".db"}}
        (some->> (get env :env-file ".env")
                 (slurp)
                 (str/split-lines)
                 (map #(re-matches #"^(.+?)=(.+)$" %))
                 (mapcat #(drop 1 %))
                 (apply hash-map))]
    {:dbtype "sqlite"
     :dbname db-file}))


(defn connect*
  "Connects and returns DB connection.

  Example:
   (connect* {:dbtype \"sqlite\" :dbname \"...\"})"
  [config]
  (jdbc/get-connection config))


(defn connect!
  "Connects to DB and updates the status.

  Examples:
   (connect!)
   (connect! {:dbtype \"sqlite\" :dbname \"...\"})"
  ([]
   (connect! (default-config)))
  ([config]
   (when-not @var/conn
     (log/info "DB config:" (update config :password #(repeat (count %) "*")))
     (reset! var/conn (connect* config)))))


(defn disconnect*
  "Disconnects from DB.

  Example:
   (disconnect* (`connect*` ...))"
  [^Connection conn]
  (.close ^Connection conn))


(defn disconnect!
  "Disconnects from DB and resets the status." []
  (when-let [x @var/conn]
    (disconnect* x)
    (reset! var/conn nil)))


(defn connecting?
  "Returns `true` if connection is alive, else returns `false`." []
  (if @var/conn
    true
    false))


(defn execute
  "Executes a query and returns result of it.

  Example:
   (execute \"SELECT * FROM table\")"
  ([query]
   (execute @var/conn query))
  ([conn query]
   ;; NOTE
   ;;  Don't catch the exception. the exception will be handled on the high level layer
   (let [opt {:return-keys true
              :builder-fn res/as-unqualified-maps}]
     (if conn
       (jdbc/execute! var/conn query opt)
       (do
         (log/error "DB is not initialized")
         (throw (ex-info "not yet ready for serving" {:type :not-ready :input [query]})))))))


(defn execute-all
  "Executes queries and returns results of them.

  Example:
   (execute-all [\"SELECT * FROM table\"])"
  ([queries]
   (execute-all @var/conn queries))
  ([conn queries]
   (execute var/conn (merge-queries queries))))


(defmacro with-transaction
  "Executes queries with transaction.

  Example:
   (with-transaction
     (`execute` \"INSERT INTO table (id) VALUES id = 1\")
     (`execute` \"INSERT INTO table (id) VALUES id = 1\") ; thorw! will rollback
     (`execute` ...))"
  [conn & xs]
  `(if ~conn
     (jdbc/with-transaction [c# ~conn] ~@xs)
     (do
       (log/error "DB is not initialized")
       (throw (ex-info "not yet ready for serving" {:type :not-ready})))))


(comment
  (connect!)
  (connecting?)
  (disconnect!)
  )
