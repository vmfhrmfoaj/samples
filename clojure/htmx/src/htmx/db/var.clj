(ns ^{:clojure.tools.namespace.repl/load   false
      :clojure.tools.namespace.repl/unload false}
  htmx.db.var)

(set! *warn-on-reflection* true)


(def conn
  "DB connection."
  (atom nil))
