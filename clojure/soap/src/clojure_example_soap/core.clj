(ns clojure-example-soap.core
  (:require [mount.core :as mount :refer [defstate]]
            [taoensso.timbre :as log])
  (:import java.io.IOException
           java.security.MessageDigest
           java.util.Base64
           javax.annotation.Resource
           javax.jws.HandlerChain
           javax.jws.WebService
           javax.jws.WebParam
           javax.xml.namespace.QName
           javax.xml.soap.SOAPException
           javax.xml.ws.Endpoint
           javax.xml.ws.WebServiceContext
           javax.xml.ws.handler.Handler
           javax.xml.ws.handler.MessageContext
           javax.xml.ws.handler.soap.SOAPHandler
           javax.xml.ws.handler.soap.SOAPMessageContext
           javax.xml.ws.soap.SOAPFaultException)
  (:gen-class))

(def ^:private wsse
  "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd")

(def ^:private wsu
  "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd")

(def ^:private known-headers
  #{(QName. wsse "Security" "wsse")})

(defn- header-name
  [elem-hdr]
  (some-> elem-hdr (.getElementName) (.getLocalName)))

(def ^:private hdr-elem=>name-&-hdr
  (juxt header-name identity))

(defn- hdr-elems=>header-info
  [iter]
  (->> iter (map hdr-elem=>name-&-hdr) (into {})))

(defn- element
  [hdr ns tag]
  (let [elem-lst (.getElementsByTagNameNS hdr ns tag)]
    (when-not (zero? (.getLength elem-lst))
      (some-> elem-lst
              (.item 0)
              (.getTextContent)))))

(def ^:private password
  ;; FIXME
  (constantly "password"))

(let [md (MessageDigest/getInstance "sha1")]
  (defn- sha1
    [str]
    (->> str (.getBytes) (.digest md))))

(let [encoder (Base64/getEncoder)]
  (defn- base64
    [str]
    (.encodeToString encoder str)))

(defn- password-digest
  [user-name random timestamp]
  (let [pwd (password user-name)]
    (->> (str random timestamp pwd)
         (sha1)
         (base64))))

(deftype SampleHandler []
  SOAPHandler
  (getHeaders [this]
    known-headers)

  Handler
  (close [this ctx])
  (handleFault [this ctx] true)
  (handleMessage [this ctx]
    (let [^SOAPMessageContext ctx ctx]
      (let [^boolean outbound? (.get ctx MessageContext/MESSAGE_OUTBOUND_PROPERTY)]
        (when (and (not (nil? outbound?))
                   (not outbound?))
          (try
            (when-let [hdr-info (some-> ctx
                                        (.getMessage)
                                        (.getSOAPPart)
                                        (.getEnvelope)
                                        (.getHeader)
                                        (.examineAllHeaderElements)
                                        (iterator-seq)
                                        (hdr-elems=>header-info))]
              (when-let [sec-hdr (hdr-info "Security")]
                (let [user-name (element sec-hdr wsse "Username")
                      pwd (element sec-hdr wsse "Password")
                      rnd (element sec-hdr wsse "Nonce")
                      timestamp (element sec-hdr wsu "Created")]
                  (when-not (and user-name
                                 pwd
                                 rnd
                                 timestamp
                                 (= pwd (password-digest user-name rnd timestamp)))
                    (let [resp (some-> ctx
                                       (.getMessage)
                                       (.getSOAPPart)
                                       (.getEnvelope)
                                       (.getBody)
                                       (.addFault)
                                       (doto (.setFaultString "Your password is wrong.")))]
                      (throw (SOAPFaultException. resp)))))))
            (catch SOAPException e
              (log/error e))
            (catch IOException e
              (log/error e)))))
      true)))

(definterface ISample
  (^String echo [^String msg]))

(deftype ^{WebService {:targetNamespace "http://soap.sample"}
           HandlerChain {:file "handler-chain.xml"}} Sample
    [^{Resource {:type WebServiceContext}
       :volatile-mutable true} ctx]
  ISample
  (echo [this ^{WebParam {:name "message"}} msg]
    msg))

(def ^:dynamic *url*
  "http://localhost:9000/Sample")

(defstate endpoint
  :start
  (do
    (log/info "Start publishing Sample at" *url*)
    (Endpoint/publish *url* (Sample. nil)))
  :stop
  (do
    (log/info "Stop publishing Sample at" *url*)
    (.stop endpoint)))

(defn -main [& _]
  (log/merge-config! {:level :warn
                      :timestamp-opts {:timezone (java.util.TimeZone/getDefault)}})
  (.addShutdownHook (Runtime/getRuntime) (Thread. #(mount/stop)))
  (mount/start))
