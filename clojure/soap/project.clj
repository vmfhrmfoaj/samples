(defproject clojure-example-soap
  "0.1.0-SNAPSHOT"

  :description "FIXME: write description"
  :url "https://gitlab.com/vmfhrmfoaj/clojure-example-soap"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :main clojure-example-soap.core

  :source-paths ["src" "spec"]

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [com.taoensso/timbre "4.10.0"]
                 [com.fzakaria/slf4j-timbre "0.3.8"]
                 [mount "0.1.12"]
                 [javax.ws.rs/javax.ws.rs-api "2.1"]]

  ;; NOTE
  ;;  this is required for Java 9 or heigher.
  :jvm-opts ["--add-modules" "java.xml.ws"]

  :profiles
  {:uberjar {:aot :all}
   :dev {:source-paths ["test" "tool"]
         :plugins [[cider/cider-nrepl "0.18.0-SNAPSHOT"]]
         :dependencies [[org.clojure/tools.nrepl "0.2.13" :exclusions [org.clojure/clojure]]
                        [tortue/spy "1.1.0" :exclusions [org.clojure/clojurescript]]]
         :repl-options
         ;; See, https://github.com/technomancy/leiningen/issues/878
         {:init [(do
                   (require 'taoensso.timbre)
                   (taoensso.timbre/merge-config! {:level :warn
                                                   :timestamp-opts {:timezone (java.util.TimeZone/getDefault)}}))]}
         ;; You can manually install a package that can handle the following options:
         ;; - https://gitlab.com/vmfhrmfoaj/lein-dev-helper
         ;; - https://gitlab.com/vmfhrmfoaj/lein-test-helper
         :auto-refresh {:on-reload "dev.tool/on-reload"}
         :test-refresh {:changes-only true
                        :timeout-in-sec 5
                        :notify-command ["terminal-notifier"
                                         "-title" "Test"
                                         "-message"]}}})
