(ns test.tool)

#?(:cljs (defmacro with-hard-redefs
           "See https://nvbn.github.io/2014/11/05/protocols-for-testing/"
           [bindings & body]
           (let [names (take-nth 2 bindings)
                 vals (take-nth 2 (drop 1 bindings))
                 tempnames (map (comp gensym name) names)
                 to-val-fns (map #(list 'identity %) names)
                 binds (map vector names vals)
                 resets (reverse (map vector names tempnames))
                 bind-value (fn [[k v]] (list 'set! k v))]
             `(let [~@(interleave tempnames to-val-fns)]
                ~@(map bind-value binds)
                (try
                  ~@body
                  (finally
                    ~@(map bind-value resets)))))))
