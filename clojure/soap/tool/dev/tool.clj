(ns dev.tool
  (:require [taoensso.timbre :as log]))

(defn on-reload
  [tracker]
  ;; NOTE
  ;; This function is called when the source file is changed.
  ;; To do this, you need to install the `lein-dev-helper` package.
  ;; You can manually install the package from the following site:
  ;; https://gitlab.com/vmfhrmfoaj/lein-dev-helper
  (log/warn "You can change this function without restarting REPL."))
