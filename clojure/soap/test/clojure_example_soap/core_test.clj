(ns clojure-example-soap.core-test
  (:require [clojure-example-soap.core :as target]
            [clojure.test :refer :all]
            [spy.core :as spy]
            [test.tool :as tool]))

(deftest a-test
  (testing "a sample test."
    (is (let [it (spy/stub nil)]
          (it 1 2 3 4)
          (spy/called-with? it 1 2 3 4)))))
