#ifndef _SAMPLE_H_
#define _SAMPLE_H_

#define TASK_COMM_LEN 16

struct event {
    __u32 pid;
    __u32 dest_ip; // NOTE: ignore IPv6 case for now
    __u16 dest_port;
    char comm[TASK_COMM_LEN];
};

#endif /* _SAMPLE_H_ */
