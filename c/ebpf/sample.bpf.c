#include "vmlinux.h"
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_core_read.h>
#include <bpf/bpf_tracing.h>
#include "sample.h"

char LICENSE[] SEC("license") = "Dual BSD/GPL";

struct {
    __uint(type, BPF_MAP_TYPE_HASH);
    __uint(max_entries, 1024);
    __type(key, u32);
    __type(value, struct sock *);
    __uint(map_flags, BPF_F_NO_PREALLOC);
} sockets SEC(".maps");

struct {
    __uint(type, BPF_MAP_TYPE_PERF_EVENT_ARRAY);
    __uint(key_size, sizeof(__u32));
    __uint(value_size, sizeof(__u32));
} events SEC(".maps");

static __always_inline int
enter_tcp_connect(struct pt_regs *ctx, struct sock *sk)
{
    __u64 pid_tgid = bpf_get_current_pid_tgid();
    __u32 pid = pid_tgid >> 32;
    __u32 tid = pid_tgid;
    __u32 uid;

    bpf_map_update_elem(&sockets, &tid, &sk, 0);
    return 0;
}

static __always_inline int
exit_tcp_connect(struct pt_regs *ctx, int ret, int ip_ver)
{
    __u64 pid_tgid = bpf_get_current_pid_tgid();
    __u32 pid = pid_tgid >> 32;
    __u32 tid = pid_tgid;
    struct event event = {.pid = pid};
    struct sock **skpp;
    struct sock *sk;

    skpp = bpf_map_lookup_elem(&sockets, &tid);
    if (!skpp)
        return 0;

    if (ret)
        goto end;

    sk = *skpp;

    bpf_get_current_comm(&event.comm, sizeof(event.comm));
    BPF_CORE_READ_INTO(&event.dest_ip,   sk, __sk_common.skc_daddr);
    BPF_CORE_READ_INTO(&event.dest_port, sk, __sk_common.skc_dport);

    bpf_perf_event_output(ctx, &events, BPF_F_CURRENT_CPU, &event, sizeof(event));

end:
    bpf_map_delete_elem(&sockets, &tid);
    return 0;
}

SEC("kprobe/tcp_v4_connect")
int BPF_KPROBE(tcp_v4_connect, struct sock *sk)
{
    return enter_tcp_connect(ctx, sk);
}

SEC("kretprobe/tcp_v4_connect")
int BPF_KRETPROBE(tcp_v4_connect_ret, int ret)
{
    return exit_tcp_connect(ctx, ret, 4);
}
