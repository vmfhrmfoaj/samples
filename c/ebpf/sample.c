#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <unistd.h>

#include <bpf/libbpf.h>
#include <sample.skel.h>
#include "sample.h"

static int libbpf_print_fn(enum libbpf_print_level level, const char *format, va_list args)
{
    return vfprintf(stderr, format, args);
}

static void handle_event(void *ctx, int cpu, void *data, __u32 data_sz)
{
    const struct event *event = data;
    struct in_addr addr;
    char addr_str[INET_ADDRSTRLEN];

    addr.s_addr = event->dest_ip;
    inet_ntop(AF_INET, &addr, addr_str, sizeof(addr_str));
    fprintf(stderr, "%d: %s, %-16s(%x)\n", event->pid, event->comm, addr_str, event->dest_ip);
}

static void handle_lost_events(void *ctx, int cpu, __u64 lost_cnt)
{
    fprintf(stderr, "Lost %llu events on CPU #%d!\n", lost_cnt, cpu);
}

int main(int argc, char **argv)
{
    struct rlimit rlim_new = {
        .rlim_cur	= RLIM_INFINITY,
        .rlim_max	= RLIM_INFINITY,
    };
    struct perf_buffer_opts pb_opts = {
        .sample_cb = handle_event,
        .lost_cb = NULL,
    };
    struct perf_buffer *pb;
    struct sample_bpf *skel;
    int err, fd;

    libbpf_set_print(libbpf_print_fn);

    if (setrlimit(RLIMIT_MEMLOCK, &rlim_new)) {
        fprintf(stderr, "Failed to increase RLIMIT_MEMLOCK limit!\n");
        exit(1);
    }

    skel = sample_bpf__open_and_load();
    if (!skel) {
        fprintf(stderr, "Failed to open and load BPF skeleton\n");
        return 1;
    }

    /* Attach tracepoint handler */
    err = sample_bpf__attach(skel);
    if (err) {
        fprintf(stderr, "Failed to attach BPF skeleton\n");
        goto cleanup;
    }

    printf("Successfully started!\n");

    fd = bpf_map__fd(skel->maps.events);
    pb = perf_buffer__new(fd, 128, &pb_opts);
    err = libbpf_get_error(pb);
    if (err) {
        pb = NULL;
        fprintf(stderr, "failed to open perf buffer: %d\n", err);
        goto cleanup;
    }

    while (1) {
        err = perf_buffer__poll(pb, 100);
        if (err < 0 && errno != EINTR) {
            fprintf(stderr, "Error polling perf buffer: %d\n", err);
            goto cleanup;
        }
    }

cleanup:
    sample_bpf__destroy(skel);
    return -err;
}
