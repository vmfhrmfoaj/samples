#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>

#define MOD_NAME "simple_module"
#define NGX_SAMPLE_MAX_BLOCK 256
#define NGX_SAMPLE_MODULE 0x53504C45 /* "SPLE" */
#define NGX_SAMPLE_MAIN_CONF 0x02000000

#define NGX_SAMPLE_CONF__DEFAULT_SAMPLE_USE "global"

static void* ngx_sample__new_conf(ngx_cycle_t *cycle);
static char* ngx_sample__init_conf(ngx_cycle_t *cycle, void *conf);
static char* ngx_sample__block(ngx_conf_t *cf, ngx_command_t *cmd, void *conf);
static void* ngx_sample_create_http_conf(ngx_conf_t *cf);
static char* ngx_sample_merge_http_conf(ngx_conf_t *cf, void *_, void *ctx);
static ngx_int_t ngx_sample_init_http_proc(ngx_cycle_t *cycle);

typedef struct ngx_sample_block_s {
    ngx_str_t name;
    ngx_uint_t xyz;
} ngx_sample_block_t;

typedef struct ngx_sample_core_conf_s {
    ngx_array_t blocks;
} ngx_sample_conf_t;

static ngx_core_module_t ngx_sample__module_ctx = {
    ngx_string("sample"),
    ngx_sample__new_conf,
    ngx_sample__init_conf
};

static ngx_command_t ngx_sample__commands[] = {
    { ngx_string("sample"),
      NGX_MAIN_CONF | NGX_CONF_BLOCK | NGX_CONF_TAKE1,
      ngx_sample__block,
      0,
      0,
      NULL },

      ngx_null_command
};

ngx_module_t ngx_sample_module = {
    NGX_MODULE_V1,
    &ngx_sample__module_ctx,    /* module context */
    ngx_sample__commands,       /* module directives */
    NGX_CORE_MODULE,            /* module type */
    NULL,                       /* init master */
    NULL,                       /* init module */
    NULL,                       /* init process */
    NULL,                       /* init thread */
    NULL,                       /* exit thread */
    NULL,                       /* exit process */
    NULL,                       /* exit master */
    NGX_MODULE_V1_PADDING
};


static ngx_core_module_t ngx_sample__core_module_ctx = {
    ngx_string("sample_core"),
    NULL,
    NULL
};

static ngx_command_t ngx_sample__core_commands[] = {
    { ngx_string("xyz"),
      NGX_SAMPLE_MAIN_CONF | NGX_CONF_TAKE1,
      ngx_conf_set_num_slot,
      0,
      offsetof(ngx_sample_block_t, xyz),
      NULL },

    ngx_null_command
};

ngx_module_t ngx_sample_core_module = {
    NGX_MODULE_V1,
    &ngx_sample__core_module_ctx, /* module context */
    ngx_sample__core_commands,    /* module directives */
    NGX_SAMPLE_MODULE,            /* module type */
    NULL,                         /* init master */
    NULL,                         /* init module */
    NULL,                         /* init process */
    NULL,                         /* init thread */
    NULL,                         /* exit thread */
    NULL,                         /* exit process */
    NULL,                         /* exit master */
    NGX_MODULE_V1_PADDING
};


typedef struct ngx_sample_http_conf_s {
    ngx_str_t sample_name;

    ngx_sample_block_t *sample;
} ngx_sample_http_conf_t;

static ngx_http_module_t ngx_sample__http_module_ctx = {
	NULL,                        /* preconfiguration */
	NULL,                        /* postconfiguration */
	NULL,                        /* create main configuration */
	NULL,                        /* init main configuration */
	ngx_sample_create_http_conf, /* create server configuration */
	ngx_sample_merge_http_conf,  /* merge server configuration */
	NULL,                        /* create location configuration */
	NULL                         /* merge location configuration */
};

static ngx_command_t ngx_sample__http_commands[] = {
    { ngx_string("sample_use"),
      NGX_HTTP_SRV_CONF | NGX_CONF_TAKE1,
      ngx_conf_set_str_slot,
      0,
      offsetof(ngx_sample_http_conf_t, sample_name),
      NULL },

    ngx_null_command
};

ngx_module_t ngx_sample_http_module = {
    NGX_MODULE_V1,
    &ngx_sample__http_module_ctx, /* module context */
    ngx_sample__http_commands,    /* module directives */
    NGX_HTTP_MODULE,              /* module type */
    NULL,                         /* init master */
    NULL,                         /* init module */
    ngx_sample_init_http_proc,    /* init process */
    NULL,                         /* init thread */
    NULL,                         /* exit thread */
    NULL,                         /* exit process */
    NULL,                         /* exit master */
    NGX_MODULE_V1_PADDING
};


static void* ngx_sample__new_conf(ngx_cycle_t *cycle)
{
    ngx_sample_conf_t *conf = NULL;
    ngx_log_error(NGX_LOG_NOTICE, cycle->log, 0, MOD_NAME ":%s", __func__);

    conf = ngx_pcalloc(cycle->pool, sizeof(*conf));
    if (conf == NULL) {
        ngx_log_error(NGX_LOG_ERR, cycle->log, 0, MOD_NAME ":%s: can't allocate memory for the core config", __func__);
        return NULL;
    }
    if (ngx_array_init(&conf->blocks, cycle->pool, NGX_SAMPLE_MAX_BLOCK, sizeof(ngx_sample_block_t)) != NGX_OK) {
        ngx_log_error(NGX_LOG_ERR, cycle->log, 0, MOD_NAME ":%s: can't allocate memory for the core config", __func__);
        goto err;
    }

    ngx_log_error(NGX_LOG_NOTICE, cycle->log, 0, MOD_NAME ":%s: sample conf=%p", __func__, conf);

    return conf;

err:
    if (conf != NULL) {
        ngx_pfree(cycle->pool, conf);
    }
    return NULL;
}

static char* ngx_sample__init_conf(ngx_cycle_t *cycle, void *conf)
{
    ngx_log_error(NGX_LOG_NOTICE, cycle->log, 0, MOD_NAME ":%s", __func__);

    return NGX_CONF_OK;
}

static char* ngx_sample__block(ngx_conf_t *cf, ngx_command_t *cmd, void *_)
{
    ngx_sample_conf_t *conf = (ngx_sample_conf_t *) ngx_get_conf(cf->cycle->conf_ctx, ngx_sample_module);
    ngx_sample_block_t *block = NULL;
    ngx_conf_t pcf;
    char *rv = NGX_CONF_ERROR;
    ngx_uint_t max_sample_mod = 0;
    int i;
    void ***ctx;
    ngx_log_error(NGX_LOG_NOTICE, cf->log, 0, MOD_NAME ":%s: conf=%p", __func__, conf);

    if (conf == NULL) {
        ngx_log_error(NGX_LOG_ERR, cf->log, 0, MOD_NAME ":%s: core_conf is null", __func__);
        return NGX_CONF_ERROR;
    }

    max_sample_mod = ngx_count_modules(cf->cycle, NGX_SAMPLE_MODULE);
    ctx = ngx_pcalloc(cf->pool, sizeof(void *));
    if (ctx == NULL) {
        return NGX_CONF_ERROR;
    }
    *ctx = ngx_pcalloc(cf->pool, max_sample_mod * sizeof(void *));
    if (*ctx == NULL) {
        return NGX_CONF_ERROR;
    }

    block = ngx_array_push(&conf->blocks);
    if (block == NULL) {
        return NGX_CONF_ERROR;
    }
    for (i = 0; cf->cycle->modules[i]; i++) {
        if (cf->cycle->modules[i]->type != NGX_SAMPLE_MODULE) {
            continue;
        }
        (*ctx)[cf->cycle->modules[i]->ctx_index] = block;
    }
    memset(block, -1, sizeof(*block));
    block->name = *((ngx_str_t *)cf->args->elts + 1);

    pcf = *cf;
    cf->ctx = ctx;
    cf->module_type = NGX_SAMPLE_MODULE;
    cf->cmd_type = NGX_SAMPLE_MAIN_CONF;
    rv = ngx_conf_parse(cf, NULL);

    *cf = pcf;
    if (rv != NGX_CONF_OK) {
        goto err;
    }

    return NGX_CONF_OK;

err:
    if (block != NULL) {
        ngx_pfree(cf->pool, block);
    }
    return rv;
}

static void* ngx_sample_create_http_conf(ngx_conf_t *cf)
{
    ngx_sample_http_conf_t *conf;
    ngx_log_error(NGX_LOG_NOTICE, cf->cycle->log, 0, MOD_NAME ":%s", __func__);

    conf = ngx_palloc(cf->pool, sizeof(*conf));
    if (conf == NULL) {
        return NULL;
    }
    memset(conf, -1, sizeof(*conf));
    conf->sample_name.len = 0;
    return conf;
}

static char* ngx_sample_merge_http_conf(ngx_conf_t *cf, void *_, void *http_conf)
{
    ngx_sample_http_conf_t *conf;
    ngx_log_error(NGX_LOG_NOTICE, cf->cycle->log, 0, MOD_NAME ":%s", __func__);

    conf = http_conf;
    if (conf == NULL) {
        return NGX_CONF_ERROR;
    }
    if (conf->sample_name.len == 0) {
        conf->sample_name.len = sizeof(NGX_SAMPLE_CONF__DEFAULT_SAMPLE_USE) - 1;
        conf->sample_name.data = (u_char*) NGX_SAMPLE_CONF__DEFAULT_SAMPLE_USE;
    }
    return NGX_CONF_OK;
}

static ngx_int_t ngx_sample_init_http_proc(ngx_cycle_t *cycle)
{
    ngx_sample_conf_t *conf;
    ngx_sample_block_t *block;
    ngx_uint_t i;
    ngx_log_error(NGX_LOG_NOTICE, cycle->log, 0, MOD_NAME ":%s", __func__);

    conf = (ngx_sample_conf_t *) ngx_get_conf(cycle->conf_ctx, ngx_sample_module);
    if (conf == NULL) {
        return NGX_ERROR;
    }
    block = conf->blocks.elts;

    for (i = 0; i < conf->blocks.nelts; i++) {
        ngx_log_error(NGX_LOG_NOTICE, cycle->log, 0, MOD_NAME
            ":%s: name=%V, xyz=%d", __func__, &block[i].name, block[i].xyz);
    }

    return NGX_OK;
}
