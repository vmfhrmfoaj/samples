(defproject html5-mediasource
  "0.1.0-SNAPSHOT"

  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :main html5-mediasource.core

  :dependencies [[org.clojure/clojure "1.9.0-alpha15"]
                 [org.clojure/clojurescript "1.9.494"]
                 [org.clojure/core.async "0.3.442"]
                 [org.clojure/data.json "0.2.6"]
                 [camel-snake-kebab "0.4.0"]
                 [compojure "1.5.2"]
                 [hiccup "1.0.5"]
                 [mount "0.1.11"]
                 [reagent "0.6.1"]
                 [ring "1.5.1"]
                 [ring/ring-defaults "0.2.2"]]

  :profiles {:dev {:auto-refresh true ; https://gitlab.com/vmfhrmfoaj/lein-dev-helper
                   :figwheel {:http-server-root "dev"
                              :reload-clj-files {:clj false :cljc true}}}}

  :cljsbuild
  {:builds {:dev {:source-paths ["src"]
                  :compiler {:main html5-mediasource.core
                             :asset-path "js/out"
                             :output-to  "resources/public/js/app.js"
                             :output-dir "resources/public/js/out"
                             :optimizations :none
                             :source-map-timestamp true
                             :warnings {:single-segment-namespace false}}
                  :figwheel {:websocket-host :js-client-host}}}}

  :clean-targets ^{:protect false} [:target-path "resources/public/js/"]
  :plugins [[lein-cljsbuild "1.1.5"]])
