(ns html5-mediasource.core
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [cljs.core.async :as a]
            [cljs.reader :refer [read-string]]
            [reagent.core :as r]))

(defn req
  [url f]
  (let [xhttp (js/XMLHttpRequest.)]
    (set! (.-onreadystatechange xhttp)
          (fn []
            (this-as this
              (when (and (= 4   (.-readyState this))
                         (= 200 (.-status this)))
                (f (.-responseText this))))))
    (.open xhttp "GET" url true)
    (.send xhttp)))

(defn req-bin
  [url f]
  (let [xhttp (js/XMLHttpRequest.)]
    (set! (.-onreadystatechange xhttp)
          (fn []
            (this-as this
              (when (and (= 4   (.-readyState this))
                         (= 200 (.-status this)))
                (f (.-response this))))))
    (.open xhttp "GET" url true)
    (set! (.-responseType xhttp) "arraybuffer")
    (.send xhttp)))

(defn video-player []
  (let [video (atom nil)]
    (fn []
      (let [ms   (js/MediaSource.)
            mime "video/mp4; codecs=\"avc1.4d401f,mp4a.40.2\""]
        (set! (.-onsourceopen ms)
              (fn [_]
                (set! (.-duration ms) 29.568)
                (let [ms-buf    (.addSourceBuffer ms mime)
                      req-ch    (a/chan)
                      update-ch (a/chan)]
                  (set! (.-mode ms-buf) "sequence")
                  (set! (.-onupdateend ms-buf) #(a/put! update-ch :updated))
                  (set! (.-onerror ms-buf) #(.log js/console %))
                  (set! (.-onabort ms-buf) #(.log js/console %))
                  (go
                    (req (str "/playback/video/sample/chunk/info") #(a/put! req-ch %))
                    (let [vd-chunk-map (read-string (<! req-ch))]
                      (go-loop [i 2]
                        (println (get-in vd-chunk-map [i :start-time]))
                        (set! (.-timestampOffset ms-buf) (get-in vd-chunk-map [i :start-time]))
                        (req-bin (str "/playback/video/sample/chunk/" i) #(a/put! req-ch %))
                        (when-let [data (<! req-ch)]
                          (.appendBuffer ms-buf data)
                          (<! update-ch)
                          (if (< i 5)
                            (recur (inc i))
                            (when-let [video @video]
                              (set! (.-currentTime video) 13)
                              (set! (.-muted video) true)
                              (let [buffered (.-buffered video)
                                    buffered (loop [acc [], i 0]
                                               (if-not (< i (.-length buffered))
                                                 acc
                                                 (recur (conj acc [(.start buffered i) (.end buffered i)])
                                                        (inc i))))]
                                (println buffered))
                              (.play video))))))))))
        [:video {:style {:background-color "#000"
                         :width 640
                         :height 360}
                 :ref (fn [elem]
                        (when elem
                          (set! (.-src elem) (.. js/window -URL (createObjectURL ms))))
                        (reset! video elem))}]))))

(enable-console-print!)
(r/render [video-player] (.querySelector js/document "#anchor"))
