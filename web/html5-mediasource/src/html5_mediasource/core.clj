(ns html5-mediasource.core
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io]
            [clojure.java.shell :refer [sh]]
            [camel-snake-kebab.core :refer [->kebab-case-keyword]]
            [compojure.core :refer [defroutes GET]]
            [compojure.route :as route]
            [hiccup.core :refer [html]]
            [mount.core :as mount :refer [defstate]]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [ring.util.response :as resp]
            [clojure.string :as str])
  (:import java.nio.file.Files))

(defn success?
  [res]
  (= 0 (:exit res)))

(defn split-video-file
  [src-path out-dir]
  (.mkdirs (io/as-file out-dir))
  (success? (sh "ffmpeg"
                "-v" "quiet"
                "-i" src-path
                "-c" "copy"
                "-map" "0"
                "-segment_time" "5"
                "-f" "segment"
                (str out-dir "/chunk_%05d.mp4"))))

(defn create-fragmented-video-file
  ([src-path]
   (create-fragmented-video-file src-path src-path))
  ([src-path target-path]
   (let [tmp (str target-path ".tmp")
         cmd ["ffmpeg"
              "-i" src-path
              "-g" "52"
              "-acodec" "copy"
              "-vcodec" "copy"
              "-f" "mp4"
              "-movflags" "empty_moov+default_base_moof+frag_keyframe"
              tmp]]
     (if (and (success? (apply sh cmd))
              (success? (sh "mv" tmp target-path)))
       true
       (do (sh "rm" tmp) nil)))))

(defn video-file->duration
  [path]
  (some-> (sh "ffprobe"
              "-v" "quiet"
              "-print_format" "json"
              "-show_format"
              path)
          :out
          (json/read-str :key-fn ->kebab-case-keyword)
          :format
          :duration
          (Double/parseDouble)))

(defn prepare-streaming []
  (let [video-dir "resources/public/video"
        out-dir   (str video-dir "/sample")]
    (split-video-file (str video-dir "/sample.mp4") out-dir)
    (->> (io/as-file out-dir)
         (file-seq)
         (map #(.getPath %))
         (filter #(str/ends-with? % ".mp4"))
         (reduce (fn [[out start-time] path]
                   (create-fragmented-video-file path)
                   (let [duration (video-file->duration path)]
                     [(conj out {:path path
                                 :start-time start-time
                                 :duration duration})
                      (+ start-time duration)]))
                 [[] 0])
         (first))))

(def sample-info (atom (prepare-streaming)))

(defroutes handler
  (GET "/" []
    (html
      [:html
       [:head
        [:title "Test for HTML5 Media Source Extensions (MSE))"]]
       [:body
        [:div#anchor "Loadding..."]
        [:script {:src "js/app.js"}]]]))
  (GET "/playback/video/sample/chunk/info" []
    (-> @sample-info
        (->> (mapv #(select-keys % [:start-time :duration])))
        (pr-str)
        (resp/response)
        (resp/content-type "application/edn")
        (resp/charset "utf-8")))
  (GET "/playback/video/sample/chunk/:chunk" [chunk]
    (-> chunk
        (Integer/parseInt)
        (@sample-info)
        :path
        (io/as-file)
        (.toPath)
        (Files/readAllBytes)
        (io/input-stream)))
  (route/resources "/")
  (route/not-found "Page not found"))

(defstate web-server
  :start (run-jetty (wrap-defaults #'handler site-defaults) {:port 3000 :join? false})
  :stop  (.stop web-server))
